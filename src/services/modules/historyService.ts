import apiService from "./apiService";
import { AxiosError, AxiosResponse, CancelToken } from "axios";
import { Users, User, HistoryForm } from "models";

type Source = {
  token: CancelToken;
};

class HistoryService {
  static storiesByPage = (page: number, source: Source) => {
    return new Promise<Users>((resolve, reject) => {
      apiService
        .get(`stories?page=${page}`, {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<Users>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static queryStoriesByPage = (page: number, query: string, source: Source) => {
    return new Promise<Users>((resolve, reject) => {
      apiService
        .post(
          `stories/query?page=${page}`,
          { attribute: query },
          {
            cancelToken: source.token,
          }
        )
        .then(
          (response: AxiosResponse<Users>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static store = (historyForm: HistoryForm, source: Source) => {
    return new Promise<User>((resolve, reject) => {
      apiService
        .post("stories", historyForm, {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<User>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static getById = (id: number, source: Source) => {
    return new Promise<User>((resolve, reject) => {
      apiService.get(`stories/${id}/edit`).then(
        (response: AxiosResponse<User>) => resolve(response?.data),
        (error: AxiosError) => reject(error)
      );
    });
  };

  static update = (historyForm: HistoryForm, source: Source) => {
    return new Promise<User>((resolve, reject) => {
      apiService.put(`stories/${historyForm.id}`, historyForm).then(
        (response: AxiosResponse<User>) => resolve(response?.data),
        (error: AxiosError) => reject(error)
      );
    });
  };

  static export = (data: { query: string }, typeFile: string) => {
    return new Promise<any>((resolve, reject) => {
      apiService
        .post(`stories/export/${typeFile}`, data, {
          responseType: "blob",
        })
        .then(
          (response: AxiosResponse<any>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static destroy = (id: number) => {
    return new Promise<User>((resolve, reject) => {
      apiService.delete(`stories/${id}`).then(
        (response: AxiosResponse<User>) => resolve(response?.data),
        (error: AxiosError) => reject(error)
      );
    });
  };
}

export default HistoryService;
