import apiService from "./apiService";
import { AxiosError, AxiosResponse, CancelToken } from "axios";
import { UserForm, User } from "models";

type Source = {
  token: CancelToken;
};

class ProfileService {
  static update = (user: UserForm, source: Source) => {
    return new Promise<User>((resolve, reject) => {
      apiService
        .put(`profile/${user.id}`, user, {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<User>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };
}

export default ProfileService;
