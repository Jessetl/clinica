import apiService from "./apiService";
import { AxiosError, AxiosResponse, CancelToken } from "axios";
import { Users, User, DoctorForm } from "models";

type Source = {
  token: CancelToken;
};

class DoctorService {
  static doctorsByPage = (page: number, source: Source) => {
    return new Promise<Users>((resolve, reject) => {
      apiService
        .get(`doctors?page=${page}`, {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<Users>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static queryDoctorsByPage = (page: number, query: string, source: Source) => {
    return new Promise<Users>((resolve, reject) => {
      apiService
        .post(
          `doctors/query?page=${page}`,
          { attribute: query },
          {
            cancelToken: source.token,
          }
        )
        .then(
          (response: AxiosResponse<Users>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static store = (doctorForm: DoctorForm, source: Source) => {
    return new Promise<User>((resolve, reject) => {
      apiService
        .post("doctors", doctorForm, {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<User>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static getById = (id: number, source: Source) => {
    return new Promise<User>((resolve, reject) => {
      apiService
        .get(`doctors/${id}/edit`, {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<User>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static status = (id: number, source: Source) => {
    return new Promise<User>((resolve, reject) => {
      apiService
        .get(`doctors/${id}/status`, {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<User>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static update = (doctorForm: DoctorForm, source: Source) => {
    return new Promise<User>((resolve, reject) => {
      apiService.put(`doctors/${doctorForm.id}`, doctorForm).then(
        (response: AxiosResponse<User>) => resolve(response?.data),
        (error: AxiosError) => reject(error)
      );
    });
  };
  static usage = (user: User) => {
    return new Promise<User>((resolve, reject) => {
      apiService
        .put(`doctors/${user.id}/usage`, {
          ...user,
        })
        .then(
          (response: AxiosResponse<User>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static export = (data: { query: string }, typeFile: string) => {
    return new Promise<any>((resolve, reject) => {
      apiService
        .post(`doctors/export/${typeFile}`, data, {
          responseType: "blob",
        })
        .then(
          (response: AxiosResponse<any>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static destroy = (id: number) => {
    return new Promise<User>((resolve, reject) => {
      apiService.delete(`doctors/${id}`).then(
        (response: AxiosResponse<User>) => resolve(response?.data),
        (error: AxiosError) => reject(error)
      );
    });
  };
}

export default DoctorService;
