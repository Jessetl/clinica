import AuthService from "./modules/authService";
import ProfileService from "./modules/profileService";
import HistoryService from "./modules/historyService";
import StateService from "./modules/stateService";
import DoctorService from "./modules/doctorService";
import FormService from "./modules/formService";
import RangeService from "./modules/rangeService";
import BoardService from "./modules/boardService";
import TestService from "./modules/testSetvice";

export {
  AuthService,
  ProfileService,
  HistoryService,
  StateService,
  DoctorService,
  FormService,
  RangeService,
  BoardService,
  TestService,
};
