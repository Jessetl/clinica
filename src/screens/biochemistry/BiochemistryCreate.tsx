import React, { ReactNode } from "react";
import { RouteComponentProps } from "react-router-dom";
import axios, { CancelTokenSource } from "axios";

import { User, Form, TestTypeForm } from "models";
import { HistoryService, BoardService, FormService } from "services";

import { initialState, State } from "./FormState";
import { Input, Button } from "components";
import { PageTitle, TitleModal } from "components";

import { handlerError, showSuccess, Globals, showError } from "utils";
import { FORM_BIOCHEMISTRY } from "utils";
import RoleId from "utils/RoleId";

import { RootState } from "reducers";
import { connect, ConnectedProps } from "react-redux";

interface MatchParams {
  id?: string;
}

const mapState = (state: RootState) => ({
  user: state.user,
  form: state.form,
});

const mapDispatch = {
  dispatchForm: (form: Form) => ({ form: form, type: "Form/SET" }),
};

const connector = connect(mapState, mapDispatch);

type Props = ConnectedProps<typeof connector> &
  RouteComponentProps<MatchParams>;

class BiochemistryCreate extends React.PureComponent<Props, State> {
  public state: State;
  public source: CancelTokenSource;

  constructor(props: Props) {
    super(props);

    this.state = initialState;

    const cancelToken = axios.CancelToken;
    this.source = cancelToken.source();
  }

  componentDidMount() {
    const cancelToken = axios.CancelToken;
    this.source = cancelToken.source();

    this.getBoards();
    this.getUserHistoryData();
  }

  componentDidUpdate(_: Props, prevState: State) {
    if (
      prevState.boards !== this.state.boards &&
      this.state.boards.length > 0
    ) {
      this.getFormType();
    }
  }

  componentWillUnmount() {
    this.source.cancel("Cancel by user");
  }

  getBoards = async () => {
    try {
      const boards = await BoardService.boards(FORM_BIOCHEMISTRY, this.source);

      this.setState({ boards });
    } catch (err) {
      handlerError(err);
    }
  };

  getUserHistoryData = async () => {
    const { id } = this.props.match.params;

    if (!!id) {
      try {
        const doctor = await HistoryService.getById(parseInt(id), this.source);
        const { person } = doctor;

        if (!!person) {
          const parseHistory: User = {
            ...doctor,
            ...person,
            id: doctor.id,
          };

          console.log("history", parseHistory);

          this.setState({
            history: parseHistory,
            chronological: person.age,
          });
        }
      } catch (err) {
        handlerError(err);
      }
    }
  };

  getFormType = () => {
    const parseForm: TestTypeForm[] = [
      {
        // 1
        name: "somatomedine",
        translate: "Somatomedina C (IGF-1) (ng/ml)",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 2
        name: "glycosylated_hemoglobin",
        translate: "Hb Glicosilada: %",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 3
        name: "basal_insulin",
        translate: "Insulina Basal",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 4
        name: "post_prandial",
        translate: "Post Prandial (mui/ml)",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 5
        name: "ratio_tg_hdl",
        translate: "Relación TG: mg/dll - HDL",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 6
        name: "dhea_so",
        translate: "DHEA: ug/dl",
        relative_value: "",
        dimensions: false,
        absolute_value: "",
      },
      {
        // 7
        name: "homocysteine",
        translate: "Homocisteina (umol/L)",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 8
        name: "psa_total",
        translate: "PSA Total / Libre (%)",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 9
        name: "fsh_ui_l",
        translate: "FSH UI/L",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 10
        name: "bone_densitometry",
        translate: "Densitometría ósea: F",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
    ];

    this.setState({ form: parseForm });
  };

  handleChangeSelect = (key: string) => {
    return (event: React.ChangeEvent<HTMLSelectElement>): void => {
      const { value, name } = event.currentTarget;
      console.log("handleChangeSelect: key:", key, name);

      const parseValue = typeof value === "string" ? parseFloat(value) : value;

      this.setState((prevState: any) => {
        return {
          ...prevState,
          [key]: parseValue || "",
          form: [],
        };
      });
    };
  };

  handleChange = (key: number, keyName?: string) => {
    return (event: React.ChangeEvent<HTMLInputElement>) => {
      const { value } = event.currentTarget;

      const copyForm = [...this.state.form];
      const parseValue = typeof value === "string" ? parseFloat(value) : value;
      const validValue = Number.isNaN(parseValue) ? "" : parseValue;

      switch (key) {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
          copyForm[key] = {
            ...copyForm[key],
            relative_value: validValue,
          };
          break;
      }

      this.setState((prevState: any) => {
        return {
          ...prevState,
          form: copyForm,
        };
      });
    };
  };

  getReverseByKey = (key: number): boolean => {
    switch (key) {
      case 0:
      case 5:
      case 7:
      case 9:
        return true;
      default:
        return false;
    }
  };

  getAbsoluteResult = (
    name: string,
    value: number,
    reverse: boolean = false
  ): number => {
    const { boards } = this.state;

    if (!!boards && boards.length > 0) {
      const b = boards
        .filter((b) => b.name === name)
        .find((b) => Globals.getReverse(b, value, reverse));

      console.log(b, "board");
      console.log(reverse, "reverse");

      if (!!b) {
        const r = b.range; // D3 - C3
        const length = Math.abs(r.min - r.max); // F3

        if (!!r) {
          const absolute = Globals.getBoardSubtraction(b); // F4
          const calc = absolute / length; // F4 / F3

          console.log(absolute, "absolute");
          console.log(calc, "calc");

          let ageIn: number = parseFloat(b.min);
          let calcIn: number = 0;

          const agesRange = Globals.getRange(r.min, r.max);
          const ages = agesRange.map((pValue: number, key: number) => {
            calcIn = key > 0 ? calc + calcIn : calcIn;

            return {
              value: ageIn + calcIn,
              age: pValue,
            };
          });

          const reverseAges = reverse ? ages.reverse() : ages;
          const lastAge = reverseAges[reverseAges.length - 1];
          const higher = value > lastAge.value ? lastAge.age : 0;

          console.log(agesRange, "agesRange");
          console.log(ages, "ages");
          console.log(value, "value");

          return (
            reverseAges.find((a) => a.value > value || value <= a.value)?.age ||
            higher
          );
        }
      }
    }

    return 0;
  };

  getResultByDimensions = (
    form: TestTypeForm,
    name: string,
    reverse: boolean = false
  ): number => {
    const { long, high } = form;

    if (!!long && !!high) {
      const average = parseFloat(((long + high) / 2).toFixed(6));

      return this.getAbsoluteResult(name, average, reverse);
    }

    return 0;
  };

  handleSubmit = async (
    event: React.FormEvent<HTMLFormElement>
  ): Promise<void> => {
    const { user, match, dispatchForm } = this.props;
    const { history, chronological } = this.state;

    event.preventDefault();

    if (!!this.state.goToBack && match.params.id) {
      return this.props.history.replace(
        `/admin/stories/${match.params.id}/edit`
      );
    }

    if (!!!this.state.isSubmit) {
      let forms = [...this.state.form];

      const testForm = forms.find((f) => {
        const dimensions =
          (!!!f.long && f.long !== 0) || (!!!f.high && f.high !== 0);

        return !!f.dimensions
          ? dimensions
          : !!!f.relative_value && f.relative_value !== 0;
      });

      if (!!testForm) {
        return showError(`${testForm.translate} es requerido para el calculo.`);
      }

      for (let i = 0; i < forms.length; i++) {
        forms[i].absolute_value = this.getAbsoluteResult(
          forms[i].name,
          forms[i].relative_value || 0,
          this.getReverseByKey(i)
        );
      }

      if (!!user) {
        const ageBiochemistry = parseInt(
          (
            forms.reduce(function (pValue: number, cValue: TestTypeForm) {
              return pValue + getTypeOfValue(cValue.absolute_value || 0);
            }, 0) / 10
          ).toFixed(0)
        );

        const differential = Math.abs(chronological - ageBiochemistry) * 1;

        this.setState({
          isSubmit: true,
          biochemistry: ageBiochemistry,
          differential: differential,
          form: forms,
        });

        FormService.storeBiochemistry(
          forms,
          {
            userId: history.id,
            chronological: chronological,
            biochemistry: ageBiochemistry,
            differential: differential,
          },
          this.source
        )
          .then((user: User) => {
            showSuccess();

            this.setState({ goToBack: true });
            dispatchForm({ remaining: user.forms });
          })
          .catch(handlerError)
          .finally(() => this.setState({ isSubmit: false }));
      }
    }
  };

  render(): ReactNode {
    const { user, match, form: reduxForm } = this.props;
    const { isSubmit, form, goToBack } = this.state;
    const { chronological, biochemistry, differential } = this.state;

    return (
      <div className="container">
        <PageTitle backUrl={`/admin/stories/${match.params.id}/edit`}>
          Registro de Test
        </PageTitle>
        <div className="row justify-content-center">
          <div className="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
            <TitleModal>Test Edad Bioquímica</TitleModal>
            {!!user && user.role !== RoleId.ROOT && (
              <p>Cantidad de usos disponibles: {reduxForm?.remaining}</p>
            )}
            <div className="card">
              <div className="card-body">
                <form
                  onSubmit={this.handleSubmit}
                  noValidate
                  autoComplete="off"
                >
                  {!!form && form.length > 0 && (
                    <React.Fragment>
                      {form.map((row, key) => {
                        return (
                          <div className="row" key={key.toString()}>
                            <div className="col-12 col-sm-12 col-md">
                              <label htmlFor={row.name}>{row.translate}</label>
                              {row.dimensions ? (
                                <div className="row">
                                  <div className="col-12 col-sm-12 col-md">
                                    <Input
                                      name={row.name}
                                      color="gray"
                                      type="number"
                                      onChange={this.handleChange(key, "long")}
                                      step="0.001"
                                      value={row.long}
                                    />
                                  </div>
                                  <div className="col-12 col-sm-12 col-md">
                                    <Input
                                      name={row.name}
                                      color="gray"
                                      type="number"
                                      onChange={this.handleChange(key, "high")}
                                      step="0.001"
                                      value={row.high}
                                    />
                                  </div>
                                </div>
                              ) : (
                                <div className="form-group">
                                  <Input
                                    name={row.name}
                                    color="gray"
                                    type="number"
                                    onChange={this.handleChange(key)}
                                    step="0.001"
                                    value={row.relative_value}
                                  />
                                </div>
                              )}
                            </div>
                            <div className="col-12 col-sm-12 col-md">
                              <div className="form-group">
                                <Input
                                  disabled
                                  labelClass="invisible"
                                  label="Resultado"
                                  color="gray"
                                  type="text"
                                  value={row.absolute_value}
                                />
                              </div>
                            </div>
                          </div>
                        );
                      })}
                    </React.Fragment>
                  )}

                  <div className="row">
                    <div className="col-12 col-sm-12 col-md">
                      <div className="form-group">
                        <Input
                          name="chronological"
                          label="Edad Cronológica"
                          color="gray"
                          type="text"
                          onChange={(
                            event: React.ChangeEvent<HTMLInputElement>
                          ): void => {
                            const { value, name } = event.currentTarget;
                            const parseValue =
                              typeof value === "string"
                                ? parseInt(value)
                                : value;

                            this.setState((prevState: any) => {
                              return {
                                ...prevState,
                                [name]: parseValue || "",
                              };
                            });
                          }}
                          value={chronological}
                        />
                      </div>
                    </div>
                    <div className="col-12 col-sm-12 col-md">
                      <div className="form-group">
                        <Input
                          disabled
                          label="Edad Bioquímica"
                          color="gray"
                          type="text"
                          value={biochemistry}
                        />
                      </div>
                    </div>
                    <div className="col-12 col-sm-12 col-md">
                      <div className="form-group">
                        <Input
                          disabled
                          label="Edad Diferencial"
                          color="gray"
                          type="text"
                          value={differential}
                        />
                      </div>
                    </div>
                  </div>

                  <div className="text-center row">
                    <div className="col col-sm">
                      <Button
                        submitted={isSubmit}
                        block
                        type="submit"
                        className="btn-orange shadow"
                      >
                        {!!goToBack ? "Salir" : "Calcular y Guardar"}
                      </Button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function getTypeOfValue(cValue: number): number {
  return typeof cValue === "string" ? parseInt(cValue) : cValue;
}

export default connector(BiochemistryCreate);
