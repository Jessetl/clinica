import { User, TestTypeForm, Board } from "models";

export type State = {
  isSubmit: boolean;
  error: string;
  boards: Board[];
  form: TestTypeForm[];
  formType: number | "";
  history: User;
  chronological: number;
  biological: number;
  differential: number;
  goToBack: boolean;
};

export const initialState: State = {
  isSubmit: false,
  error: "",
  boards: [],
  form: [],
  formType: "",
  history: {
    id: 0,
    alphanumeric: "",
    user_id: 0,
    role: 0,
    email: "",
    coach: 0,
    forms: 0,
    status: 0,
    activated: 0,
    remember_token: "",
    created_at: new Date(),
    updated_at: new Date(),
    deleted_at: new Date(),
  },
  chronological: 0,
  biological: 0,
  differential: 0,
  goToBack: false,
};
