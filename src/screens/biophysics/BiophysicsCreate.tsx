import React, { ReactNode } from "react";
import { RouteComponentProps } from "react-router-dom";
import axios, { CancelTokenSource } from "axios";

import { User, Form, TestTypeForm } from "models";
import { HistoryService, BoardService, FormService } from "services";

import { initialState, State } from "./FormState";
import { Input, Button, Select } from "components";
import { PageTitle, TitleModal } from "components";

import { Globals, handlerError, showError, showSuccess } from "utils";
import { GENDERS, FORM_BIOPHYSICS } from "utils";
import { GENDER_FEMALE, GENDER_SPORTY_FEMALE } from "utils";
import { GENDER_MALE, GENDER_SPORTY_MALE } from "utils";
import RoleId from "utils/RoleId";

import { RootState } from "reducers";
import { ConnectedProps, connect } from "react-redux";

interface MatchParams {
  id?: string;
}

const mapState = (state: RootState) => ({
  user: state.user,
  form: state.form,
});

const mapDispatch = {
  dispatchForm: (form: Form) => ({ form: form, type: "Form/SET" }),
};

const connector = connect(mapState, mapDispatch);

type Props = ConnectedProps<typeof connector> &
  RouteComponentProps<MatchParams>;

class BiophysicsCreate extends React.PureComponent<Props, State> {
  public state: State;
  public source: CancelTokenSource;

  constructor(props: Props) {
    super(props);

    this.state = initialState;

    const cancelToken = axios.CancelToken;
    this.source = cancelToken.source();
  }

  componentDidMount() {
    const cancelToken = axios.CancelToken;
    this.source = cancelToken.source();

    this.getBoards();
    this.getUserHistoryData();
  }

  componentDidUpdate(_: Props, prevState: State) {
    if (prevState.formType !== this.state.formType && this.state.formType) {
      this.getFormType(this.state.formType);
    }
  }

  componentWillUnmount() {
    this.source.cancel("Cancel by user");
  }

  getBoards = async () => {
    try {
      const boards = await BoardService.boards(FORM_BIOPHYSICS, this.source);

      this.setState({ boards });
    } catch (err) {
      handlerError(err);
    }
  };

  getUserHistoryData = async () => {
    const { id } = this.props.match.params;

    if (!!id) {
      try {
        const doctor = await HistoryService.getById(parseInt(id), this.source);
        const { person } = doctor;

        if (!!person) {
          const parseHistory: User = {
            ...doctor,
            ...person,
            id: doctor.id,
          };

          console.log("history", parseHistory);

          this.setState({
            history: parseHistory,
            chronological: person.age,
            formType: person.gender,
          });
        }
      } catch (err) {
        handlerError(err);
      }
    }
  };

  getPulseName = (formType: number): string => {
    switch (formType) {
      case GENDER_FEMALE:
      case GENDER_MALE:
        return "normal_resting_pulse";
      case GENDER_SPORTY_FEMALE:
      case GENDER_SPORTY_MALE:
        return "sportsmen_resting_pulse";
      default:
        return "";
    }
  };

  getFatName = (formType: number): string => {
    switch (formType) {
      case GENDER_FEMALE:
        return "female_fat";
      case GENDER_SPORTY_FEMALE:
        return "sporty_female_fat";
      case GENDER_MALE:
        return "male_fat";
      case GENDER_SPORTY_MALE:
        return "sporty_male_fat";
      default:
        return "";
    }
  };

  getFormType = (formType: number) => {
    const fatName = this.getFatName(formType);
    const pulseName = this.getPulseName(formType);

    const parseForm: TestTypeForm[] = [
      {
        name: fatName,
        translate: "% Grasa",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        name: "body_mass",
        translate: "Índice de masa corporal",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        name: "digital_reflections",
        translate: "Reflejos Digitales (cm)",
        relative_value: "",
        dimensions: true,
        long: "",
        high: "",
        width: "",
        absolute_value: "",
      },
      {
        name: "visual_accommodation",
        translate: "Acomodación Visual (cm)",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        name: "static_balance",
        translate: "Balance Estático (seg)",
        dimensions: true,
        long: "",
        high: "",
        width: "",
        relative_value: "",
        absolute_value: "",
      },
      {
        name: "quaten_hydration",
        translate: "Hidratación Cuatena (seg)",
        relative_value: "",
        dimensions: false,
        absolute_value: "",
      },
      {
        name: "systolic_blood_pressure",
        translate: "Sistólica",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        name: "diastolic_blood_pressure",
        translate: "Diastólica",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        name: pulseName,
        translate: "Pulso reposo",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
    ];

    this.setState({ form: parseForm });
  };

  handleChangeSelect = (key: string) => {
    return (event: React.ChangeEvent<HTMLSelectElement>): void => {
      const { value, name } = event.currentTarget;
      console.log("handleChangeSelect: key:", key, name);

      const parseValue = typeof value === "string" ? parseInt(value) : value;

      this.setState((prevState: any) => {
        return {
          ...prevState,
          [key]: parseValue || "",
          form: [],
        };
      });
    };
  };

  handleChange = (key: number, keyName?: string) => {
    return (event: React.ChangeEvent<HTMLInputElement>) => {
      const { value } = event.currentTarget;

      const copyForm = [...this.state.form];
      const parseValue = typeof value === "string" ? parseFloat(value) : value;
      const validValue = Number.isNaN(parseValue) ? "" : parseValue;

      switch (key) {
        case 0:
        case 1:
        case 3:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
          copyForm[key] = {
            ...copyForm[key],
            relative_value: validValue,
          };
          break;
        case 2:
        case 4:
          if (!!keyName) {
            copyForm[key] = {
              ...copyForm[key],
              [keyName]: validValue,
            };
          }
          break;
      }

      this.setState((prevState: any) => {
        return {
          ...prevState,
          form: copyForm,
        };
      });
    };
  };

  getReverseByKey = (key: number): boolean => {
    switch (key) {
      case 2:
      case 4:
        return true;
      default:
        return false;
    }
  };

  getAbsoluteResult = (
    name: string,
    value: number,
    reverse: boolean = false
  ): number => {
    const { boards } = this.state;

    if (!!boards && boards.length > 0) {
      const b = boards
        .filter((b) => b.name === name)
        .find((b) => Globals.getReverse(b, value, reverse)); // D4 - C4

      console.log(
        boards.filter((b) => b.name === name),
        "boards"
      );
      console.log(b, "board");

      if (!!b) {
        const r = b.range; // D3 - C3
        const length = Math.abs(r.min - r.max); // F3

        if (!!r) {
          const absolute = Globals.getBoardSubtraction(b); // F4
          const calc = absolute / length; // F4 / F3

          console.log(name, "name");
          console.log(absolute, "absolute");
          console.log(calc, "calc");

          let ageIn: number = parseFloat(b.min);
          let calcIn: number = 0;

          const agesRange = Globals.getRange(r.min, r.max);
          const ages = agesRange.map((pValue: number, key: number) => {
            calcIn = key > 0 ? calc + calcIn : calcIn;

            return {
              value: ageIn + calcIn,
              age: pValue,
            };
          });

          const reverseAges = reverse ? ages.reverse() : ages;
          const lastAge = reverseAges[reverseAges.length - 1];
          const higher = value > lastAge.value ? lastAge.age : 0;

          console.log(agesRange, "agesRange");
          console.log(reverseAges, "ages");
          console.log(value, "value");

          return (
            reverseAges.find((a) => a.value > value || value <= a.value)?.age ||
            higher
          );
        }
      }
    }

    return 0;
  };

  getDimensionsResult = (
    form: TestTypeForm,
    name: string,
    reverse: boolean = false
  ): number => {
    const { high, long, width } = form;

    if (!!high && !!long && !!width) {
      const average = parseFloat(((high + long + width) / 3).toFixed(6));

      return this.getAbsoluteResult(name, average, reverse);
    }

    return 0;
  };

  handleSubmit = async (
    event: React.FormEvent<HTMLFormElement>
  ): Promise<void> => {
    const { user, match, dispatchForm } = this.props;
    const { formType, history, chronological } = this.state;

    event.preventDefault();

    if (!!this.state.goToBack && match.params.id) {
      return this.props.history.replace(
        `/admin/stories/${match.params.id}/edit`
      );
    }

    if (!!!this.state.isSubmit) {
      let forms = [...this.state.form];

      const testForm = forms.find((f) => {
        const dimensions =
          (!!!f.long && f.long !== 0) ||
          (!!!f.high && f.high !== 0) ||
          (!!!f.width && f.width !== 0);

        return !!f.dimensions
          ? dimensions
          : !!!f.relative_value && f.relative_value !== 0;
      });

      if (!!testForm) {
        return showError(`${testForm.translate} es requerido para el calculo.`);
      }

      for (let i = 0; i < forms.length; i++) {
        forms[i].absolute_value = forms[i].dimensions
          ? this.getDimensionsResult(
              forms[i],
              forms[i].name,
              this.getReverseByKey(i)
            )
          : this.getAbsoluteResult(
              forms[i].name,
              forms[i].relative_value || 0,
              this.getReverseByKey(i)
            );
      }

      if (!!user) {
        const ageBiological = parseInt(
          (
            forms.reduce(function (pValue: number, cValue: TestTypeForm) {
              return pValue + getTypeOfValue(cValue.absolute_value || 0);
            }, 0) / 9
          ).toFixed(0)
        );

        const differential = Math.abs(chronological - ageBiological) * 1;

        this.setState((prevState: any) => {
          return {
            ...prevState,
            isSubmit: true,
            biological: ageBiological,
            differential: differential,
            form: forms,
          };
        });

        FormService.storeBiophysics(
          forms,
          {
            userId: history.id,
            chronological: chronological,
            biological: ageBiological,
            differential: differential,
            gender: formType || 0,
          },
          this.source
        )
          .then((user: User) => {
            showSuccess();

            this.setState({ goToBack: true });
            dispatchForm({ remaining: user.forms });
          })
          .catch(handlerError)
          .finally(() => this.setState({ isSubmit: false }));
      }
    }
  };

  render(): ReactNode {
    const { user, match, form: reduxForm } = this.props;
    const { isSubmit, form, formType, goToBack } = this.state;
    const { chronological, biological, differential } = this.state;

    return (
      <div className="container">
        <PageTitle backUrl={`/admin/stories/${match.params.id}/edit`}>
          Registro de Test
        </PageTitle>
        <div className="row justify-content-center">
          <div className="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
            <TitleModal>Test Edad Biofísica</TitleModal>
            {!!user && user.role !== RoleId.ROOT && (
              <p>Cantidad de usos disponibles: {reduxForm?.remaining}</p>
            )}
            <div className="card">
              <div className="card-body">
                <form
                  onSubmit={this.handleSubmit}
                  noValidate
                  autoComplete="off"
                >
                  <div className="row">
                    <div className="col-12 col-sm-12 col-md">
                      <Select
                        name="formType"
                        color="gray"
                        options={GENDERS}
                        label="Género"
                        onChange={this.handleChangeSelect("formType")}
                        valueSelect={formType}
                      />
                    </div>
                  </div>

                  {!!form && form.length > 0 && (
                    <React.Fragment>
                      {form.map((row, key) => {
                        return (
                          <div className="row" key={key.toString()}>
                            <div className="col-12 col-sm-12 col-md">
                              <label htmlFor={row.name}>{row.translate}</label>
                              {row.dimensions ? (
                                <div className="row">
                                  <div className="col-12 col-sm-12 col-md">
                                    <Input
                                      name={row.name}
                                      color="gray"
                                      type="number"
                                      onChange={this.handleChange(key, "high")}
                                      value={row.high}
                                    />
                                  </div>
                                  <div className="col-12 col-sm-12 col-md">
                                    <Input
                                      name={row.name}
                                      color="gray"
                                      type="number"
                                      onChange={this.handleChange(key, "long")}
                                      step="0.001"
                                      value={row.long}
                                    />
                                  </div>
                                  <div className="col-12 col-sm-12 col-md">
                                    <Input
                                      name={row.name}
                                      color="gray"
                                      type="number"
                                      onChange={this.handleChange(key, "width")}
                                      step="0.001"
                                      value={row.width}
                                    />
                                  </div>
                                </div>
                              ) : (
                                <div className="form-group">
                                  <Input
                                    name={row.name}
                                    color="gray"
                                    type="number"
                                    onChange={this.handleChange(key)}
                                    step="0.001"
                                    value={row.relative_value}
                                  />
                                </div>
                              )}
                            </div>
                            <div className="col-12 col-sm-12 col-md">
                              <div className="form-group">
                                <Input
                                  disabled
                                  labelClass="invisible"
                                  label="Resultado"
                                  color="gray"
                                  type="text"
                                  value={row.absolute_value}
                                />
                              </div>
                            </div>
                          </div>
                        );
                      })}
                    </React.Fragment>
                  )}

                  <div className="row">
                    <div className="col-12 col-sm-12 col-md">
                      <div className="form-group">
                        <Input
                          name="chronological"
                          label="Edad Cronológica"
                          color="gray"
                          type="text"
                          onChange={(
                            event: React.ChangeEvent<HTMLInputElement>
                          ): void => {
                            const { value, name } = event.currentTarget;
                            const parseValue =
                              typeof value === "string"
                                ? parseInt(value)
                                : value;

                            this.setState((prevState: any) => {
                              return {
                                ...prevState,
                                [name]: parseValue || "",
                              };
                            });
                          }}
                          value={chronological}
                        />
                      </div>
                    </div>
                    <div className="col-12 col-sm-12 col-md">
                      <div className="form-group">
                        <Input
                          disabled
                          label="Edad Biofísica"
                          color="gray"
                          type="text"
                          value={biological}
                        />
                      </div>
                    </div>
                    <div className="col-12 col-sm-12 col-md">
                      <div className="form-group">
                        <Input
                          disabled
                          label="Edad Diferencial"
                          color="gray"
                          type="text"
                          value={differential}
                        />
                      </div>
                    </div>
                  </div>

                  <div className="text-center my-1 row">
                    <div className="col col-sm">
                      <Button
                        disabled={!!!formType}
                        submitted={isSubmit}
                        block
                        type="submit"
                        className="btn-orange shadow"
                      >
                        {!!goToBack ? "Salir" : "Calcular y Guardar"}
                      </Button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function getTypeOfValue(cValue: number): number {
  return typeof cValue === "string" ? parseInt(cValue) : cValue;
}

export default connector(BiophysicsCreate);
