import React, { ReactNode } from "react";
import { RouteComponentProps } from "react-router-dom";
import axios, { CancelTokenSource } from "axios";
import moment from "moment";

import { HistoryForm } from "models";
import { HistoryService, StateService } from "services";

import { initialState, State, todaySubtract } from "./FormState";
import { Select, Input, DatePicker, Button } from "components";
import { PageTitle, TitleModal } from "components";
import { NATIONALITIES, GENDER_SPORTY } from "utils";
import { MARITAL_STATUS, CELLPHONE_CODE } from "utils";
import { handlerError, showSuccess } from "utils";

import { RootState } from "reducers";
import { connect, ConnectedProps } from "react-redux";
import RoleId from "utils/RoleId";

interface MatchParams {
  id?: string;
}

const mapState = (state: RootState) => ({
  user: state.user,
  form: state.form,
});

const connector = connect(mapState);

type Props = ConnectedProps<typeof connector> &
  RouteComponentProps<MatchParams>;

class HistoryEdit extends React.PureComponent<Props, State> {
  public state: State;
  public source: CancelTokenSource;

  constructor(props: Props) {
    super(props);

    this.state = initialState;

    const cancelToken = axios.CancelToken;
    this.source = cancelToken.source();
  }

  componentDidMount() {
    const cancelToken = axios.CancelToken;
    this.source = cancelToken.source();

    this.getStateAndCity();
  }

  componentDidUpdate(_: Props, prevState: State) {
    if (
      prevState.form.state_id !== this.state.form.state_id &&
      !!this.state.form.state_id
    ) {
      this.getCityByState(this.state.form.state_id);
    }
  }

  componentWillUnmount() {
    this.source.cancel("Cancel by user");
  }

  getStateAndCity = () => {
    const { id } = this.props.match.params;

    StateService.getStateAndCity(this.source)
      .then((state) => {
        this.setState({ state }, () => {
          if (!!id) {
            this.getHistoryData();
          }
        });
      })
      .catch((err) => handlerError(err));
  };

  getHistoryData = async () => {
    const { id } = this.props.match.params;
    if (!!id) {
      try {
        const history = await HistoryService.getById(parseInt(id), this.source);
        const { person } = history;

        if (!!person) {
          const age = moment().diff(
            new Date(Date.parse(person.birthday.toString())),
            "years",
            false
          );

          const parseHistory: HistoryForm = {
            ...history,
            ...person,
            id: history.id,
            state: "",
            city: "",
            state_id: person.city.state_id,
            city_id: person.city_id,
            history: new Date(Date.parse(person.history.toString())),
            birthday: new Date(Date.parse(person.birthday.toString())),
            age: age,
          };

          console.log("history", parseHistory);

          this.setState({
            form: parseHistory,
          });
        }
      } catch (err) {
        handlerError(err);
      }
    }
  };

  getCityByState = (id: number) => {
    const stateId = typeof id === "string" ? parseInt(id) : id;

    const cities =
      this.state.state.find(({ id }) => id === stateId)?.cities || [];

    this.setState({ city: cities });
  };

  handleChangeSelect = (key: string) => {
    return (event: React.ChangeEvent<HTMLSelectElement>): void => {
      const { value, name } = event.currentTarget;
      console.log("handleChangeSelect: key:", key, name);

      this.setState((prevState: any) => {
        return {
          ...prevState,
          [key]: {
            ...prevState[key],
            [name]: value,
          },
        };
      });
    };
  };

  handleChange = (key: string) => {
    return (event: React.ChangeEvent<HTMLInputElement>) => {
      const { value, name } = event.currentTarget;
      this.setState((prevState: any) => {
        return {
          ...prevState,
          [key]: {
            ...prevState[key],
            [name]: value,
          },
        };
      });
    };
  };

  handleChangeDatePicker = (key: string, value: Date) => {
    this.setState((prevState: any) => {
      return {
        ...prevState,
        [key]: {
          ...prevState[key],
          history: value,
        },
      };
    });
  };

  handleChangeBirthday = (key: string, name: string, value: Date) => {
    const age = moment().diff(value, "years", false);

    this.setState((prevState: any) => {
      return {
        ...prevState,
        [key]: {
          ...prevState[key],
          [name]: value,
          age: age,
        },
      };
    });
  };

  handleSubmit = async (
    event: React.FormEvent<HTMLFormElement>
  ): Promise<void> => {
    event.preventDefault();

    if (this.state.isSubmit) {
      return;
    }

    this.setState({
      isSubmit: true,
    });

    const form: HistoryForm = {
      ...this.state.form,
    };

    console.log("handleSubmit form: ", form);

    HistoryService.update(form, this.source)
      .then(() => {
        showSuccess();

        setTimeout(() => {
          this.props.history.push("/admin/stories");
        }, 1000);
      })
      .catch(handlerError)
      .finally(() => this.setState({ isSubmit: false }));
  };

  render(): ReactNode {
    const { user } = this.props;
    const { isSubmit, form, state, city } = this.state;

    return (
      <div className="container">
        <PageTitle backUrl="/admin/stories">Edición de Historia</PageTitle>
        <div className="row justify-content-center">
          <div className="col">
            <TitleModal>Editar Historia</TitleModal>
            <div className="row">
              <div className="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9">
                <div className="card">
                  <div className="card-body">
                    <form
                      onSubmit={this.handleSubmit}
                      noValidate
                      autoComplete="off"
                    >
                      <div className="row">
                        <div className="col-12 col-sm-12 col-md">
                          <Select
                            name="document"
                            color="gray"
                            options={NATIONALITIES}
                            label="Nacionalidad"
                            onChange={this.handleChangeSelect("form")}
                            valueSelect={form.document}
                          />
                        </div>
                        <div className="col-12 col-sm-12 col-md">
                          <Input
                            color="gray"
                            name="identification_id"
                            label="Identificación"
                            type="number"
                            onChange={this.handleChange("form")}
                            value={form.identification_id}
                          />
                        </div>
                        <div className="col-12 col-sm-12 col-md">
                          <DatePicker
                            label="Fecha Historia"
                            name="history"
                            onChange={(value) =>
                              this.handleChangeDatePicker(
                                "form",
                                value || new Date()
                              )
                            }
                            maxDate={new Date()}
                            value={form.history}
                          />
                        </div>
                        <div className="col-12 col-sm-12 col-md">
                          <Input
                            color="gray"
                            name="surnames"
                            label="Apellidos"
                            type="text"
                            onChange={this.handleChange("form")}
                            maxlength={90}
                            value={form.surnames}
                          />
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-12 col-sm-12 col-md">
                          <Input
                            color="gray"
                            name="names"
                            label="Nombres"
                            type="text"
                            onChange={this.handleChange("form")}
                            maxlength={90}
                            value={form.names}
                          />
                        </div>
                        <div className="col-12 col-sm-12 col-md">
                          <DatePicker
                            label="Fecha Nacimiento"
                            name="birthday"
                            onChange={(value) =>
                              this.handleChangeBirthday(
                                "form",
                                "birthday",
                                value || new Date()
                              )
                            }
                            maxDate={todaySubtract}
                            value={form.birthday}
                          />
                        </div>
                        <div className="col-12 col-sm-12 col-md">
                          <Input
                            color="gray"
                            name="age"
                            label="Edad Cronológica"
                            type="number"
                            onChange={this.handleChange("form")}
                            maxlength={90}
                            value={form.age}
                          />
                        </div>
                        <div className="col-12 col-sm-12 col-md">
                          <Select
                            name="gender"
                            color="gray"
                            options={GENDER_SPORTY}
                            label="Género"
                            onChange={this.handleChangeSelect("form")}
                            valueSelect={form.gender}
                          />
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-12 col-sm-12 col-md">
                          <Input
                            color="gray"
                            name="birthplace"
                            label="Lugar Nacimiento"
                            type="text"
                            onChange={this.handleChange("form")}
                            maxlength={90}
                            value={form.birthplace}
                          />
                        </div>
                        <div className="col-12 col-sm-12 col-md">
                          <Select
                            name="phone_code"
                            color="gray"
                            options={CELLPHONE_CODE}
                            label="Código de Teléfono"
                            onChange={this.handleChangeSelect("form")}
                            valueSelect={form.phone_code}
                          />
                        </div>
                        <div className="col-12 col-sm-12 col-md">
                          <Input
                            color="gray"
                            name="phone"
                            label="Teléfono"
                            type="number"
                            onChange={this.handleChange("form")}
                            value={form.phone}
                          />
                        </div>
                        <div className="col-12 col-sm-12 col-md">
                          <Select
                            name="marital_status"
                            color="gray"
                            options={MARITAL_STATUS}
                            label="Edo. Civil"
                            onChange={this.handleChangeSelect("form")}
                            valueSelect={form.marital_status}
                          />
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-12 col-sm-12 col-md">
                          <Input
                            color="gray"
                            name="occupation"
                            label="Profesión"
                            type="text"
                            onChange={this.handleChange("form")}
                            maxlength={90}
                            value={form.occupation}
                          />
                        </div>
                        <div className="col-12 col-sm-12 col-md">
                          <Select
                            name="state_id"
                            color="gray"
                            options={state.map(({ id, name }) => ({
                              value: id,
                              label: name,
                            }))}
                            label="Estado"
                            onChange={this.handleChangeSelect("form")}
                            valueSelect={form.state_id}
                          />
                        </div>
                        <div className="col-12 col-sm-12 col-md">
                          <Select
                            name="city_id"
                            color="gray"
                            options={city.map(({ id, name }) => ({
                              value: id,
                              label: name,
                            }))}
                            label="Ciudad"
                            onChange={this.handleChangeSelect("form")}
                            valueSelect={form.city_id}
                          />
                        </div>
                        <div className="col-12 col-sm-12 col-md">
                          <Input
                            color="gray"
                            name="address"
                            label="Dirección"
                            type="text"
                            onChange={this.handleChange("form")}
                            maxlength={90}
                            value={form.address}
                          />
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-12 col-sm-12 col-md">
                          <Input
                            color="gray"
                            name="email"
                            label="E-mail"
                            type="text"
                            onChange={this.handleChange("form")}
                            maxlength={90}
                            value={form.email}
                          />
                        </div>
                        <div className="col-12 col-sm-12 col-md">
                          <Input
                            color="gray"
                            name="observations"
                            label="Observaciones"
                            type="text"
                            onChange={this.handleChange("form")}
                            maxlength={90}
                            value={form.observations}
                          />
                        </div>
                      </div>
                      <div className="text-center row">
                        <div className="col col-sm">
                          <Button
                            submitted={isSubmit}
                            block
                            type="submit"
                            className="btn-orange shadow"
                          >
                            Guardar
                          </Button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              {!!user && (
                <div className="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                  {(user.role === RoleId.ROOT ||
                    (!!this.props.form && this.props.form.remaining > 0)) && (
                    <div className="row">
                      <div className="col">
                        <div className="form-group">
                          <Button
                            onClick={() =>
                              this.props.history.push(
                                `/admin/biophysics/${form.id}/create`
                              )
                            }
                            block
                            type="button"
                            outline="orange"
                            className="shadow"
                          >
                            Edad Biofísica
                          </Button>
                        </div>
                      </div>
                    </div>
                  )}

                  {(user.role === RoleId.ROOT ||
                    (user.role === RoleId.DOCTOR &&
                      !!this.props.form &&
                      this.props.form.remaining > 0)) && (
                    <div className="row">
                      <div className="col">
                        <div className="form-group">
                          <Button
                            onClick={() =>
                              this.props.history.push(
                                `/admin/biochemistry/${form.id}/create`
                              )
                            }
                            block
                            type="button"
                            outline="orange"
                            className="shadow"
                          >
                            Edad Bioquímica
                          </Button>
                        </div>
                      </div>
                    </div>
                  )}

                  {(user.role === RoleId.ROOT ||
                    (user.role === RoleId.DOCTOR &&
                      !!this.props.form &&
                      this.props.form.remaining > 0)) && (
                    <div className="row">
                      <div className="col">
                        <div className="form-group">
                          <Button
                            onClick={() =>
                              this.props.history.push(
                                `/admin/orthomolecular/${form.id}/create`
                              )
                            }
                            block
                            type="button"
                            outline="orange"
                            className="shadow"
                          >
                            Edad Ortomolecular
                          </Button>
                        </div>
                      </div>
                    </div>
                  )}

                  {(user.role === RoleId.ROOT ||
                    (user.role === RoleId.DOCTOR &&
                      !!this.props.form &&
                      this.props.form.remaining > 0)) && (
                    <div className="row">
                      <div className="col">
                        <div className="form-group">
                          <Button
                            onClick={() =>
                              this.props.history.push(
                                `/admin/genetics/${form.id}/create`
                              )
                            }
                            block
                            type="button"
                            outline="orange"
                            className="shadow"
                          >
                            Edad Genética
                          </Button>
                        </div>
                      </div>
                    </div>
                  )}

                  {user.role !== RoleId.ROOT &&
                    !!this.props.form &&
                    this.props.form.remaining <= 0 && (
                      <div className="row">
                        <div className="col">
                          <div className="form-group">
                            <Button
                              block
                              type="button"
                              outline="orange"
                              className="shadow"
                            >
                              No posee cupos disponibles para realizar test
                            </Button>
                          </div>
                        </div>
                      </div>
                    )}
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connector(HistoryEdit);
