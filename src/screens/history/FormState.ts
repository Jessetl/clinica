import { HistoryForm, State as States, City, User } from "models";
import moment from "moment";

export type State = {
  isSubmit: boolean;
  error: string;
  state: States[];
  city: City[];
  form: HistoryForm;
  statecity: boolean;
};

export type UserProps = {
  user: User;
  onEdit: (id: number) => void;
  onDelete: (id: number) => void;
  toForms: (id: number) => void;
};

export const today = moment().toDate();
export const todaySubtract = moment().subtract(18, "year").toDate();

export const initialState: State = {
  isSubmit: false,
  error: "",
  state: [],
  city: [],
  form: {
    id: "",
    user_id: "",
    state_id: "",
    city_id: "",
    state: "",
    city: "",
    document: "",
    identification_id: "",
    history: today,
    surnames: "",
    names: "",
    birthday: todaySubtract,
    age: "",
    gender: "",
    birthplace: "",
    phone_code: "",
    phone: "",
    cellphone_code: "",
    cellphone: "",
    marital_status: "",
    occupation: "",
    address: "",
    email: "",
    observations: "",
  },
  statecity: false,
};
