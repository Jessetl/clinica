import React, { ReactNode } from "react";
import axios, { CancelTokenSource } from "axios";
import { RouteComponentProps } from "react-router-dom";
import moment from "moment";

import { HistoryForm } from "models";
import { HistoryService, StateService } from "services";

import { initialState, State, today, todaySubtract } from "./FormState";
import { Select, Input, DatePicker, Button } from "components";
import { TitleModal, PageTitle } from "components";
import { NATIONALITIES, GENDER_SPORTY } from "utils";
import { MARITAL_STATUS, CELLPHONE_CODE } from "utils";
import { handlerError, showSuccess } from "utils";

type Props = RouteComponentProps;

class HistoryCreate extends React.PureComponent<Props, State> {
  public state: State;
  public source: CancelTokenSource;

  constructor(props: Props) {
    super(props);

    this.state = initialState;

    const cancelToken = axios.CancelToken;
    this.source = cancelToken.source();
  }

  componentDidMount() {
    const cancelToken = axios.CancelToken;
    this.source = cancelToken.source();

    this.getStateAndCity();
  }

  componentDidUpdate(_: Props, prevState: State) {
    if (
      prevState.form.state_id !== this.state.form.state_id &&
      this.state.form.state_id
    ) {
      this.getCityByState(this.state.form.state_id);
    }
  }

  componentWillUnmount() {
    this.source.cancel("Cancel by user");
  }

  getStateAndCity = () => {
    StateService.getStateAndCity(this.source)
      .then((state) => {
        this.setState({ state });
      })
      .catch((err) => handlerError(err));
  };

  getCityByState = (id: number) => {
    const stateId = typeof id === "string" ? parseInt(id) : id;
    const cities =
      this.state.state.find(({ id }) => id === stateId)?.cities || [];

    this.setState({ city: cities });
  };

  handleChangeSelect = (key: string) => {
    return (event: React.ChangeEvent<HTMLSelectElement>): void => {
      const { value, name } = event.currentTarget;
      console.log("handleChangeSelect: key:", key, name);

      this.setState((prevState: any) => {
        return {
          ...prevState,
          [key]: {
            ...prevState[key],
            [name]: value,
          },
        };
      });
    };
  };

  handleChange = (key: string) => {
    return (event: React.ChangeEvent<HTMLInputElement>) => {
      const { value, name } = event.currentTarget;
      this.setState((prevState: any) => {
        return {
          ...prevState,
          [key]: {
            ...prevState[key],
            [name]: value,
          },
        };
      });
    };
  };

  handleChangeDatePicker = (key: string, name: string, value: Date) => {
    this.setState((prevState: any) => {
      return {
        ...prevState,
        [key]: {
          ...prevState[key],
          [name]: value,
        },
      };
    });
  };

  handleChangeBirthday = (key: string, name: string, value: Date) => {
    const age = moment().diff(value, "years", false);

    this.setState((prevState: any) => {
      return {
        ...prevState,
        [key]: {
          ...prevState[key],
          [name]: value,
          age: age,
        },
      };
    });
  };

  handleSubmit = async (
    event: React.FormEvent<HTMLFormElement>
  ): Promise<void> => {
    event.preventDefault();

    if (!!!this.state.isSubmit) {
      this.setState({ isSubmit: true });

      const form: HistoryForm = {
        ...this.state.form,
      };

      if (!!!this.state.statecity) {
        delete form.state;
        delete form.city;
      } else {
        delete form.state_id;
        delete form.city_id;
      }

      console.log("handleSubmit form: ", form);

      HistoryService.store(form, this.source)
        .then(() => {
          showSuccess();

          setTimeout(() => {
            this.props.history.push("/admin/stories");
          }, 1000);
        })
        .catch(handlerError)
        .finally(() => this.setState({ isSubmit: false }));
    }
  };

  render(): ReactNode {
    const { isSubmit, form, state, city } = this.state;
    const { statecity } = this.state;

    return (
      <div className="container">
        <PageTitle backUrl="/admin/stories">Registro de Historia</PageTitle>
        <div className="row justify-content-center">
          <div className="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
            <TitleModal>Nueva Historia</TitleModal>
            <div className="card">
              <div className="card-body">
                <form
                  onSubmit={this.handleSubmit}
                  noValidate
                  autoComplete="off"
                >
                  <div className="row">
                    <div className="col-12 col-sm-12 col-md">
                      <Select
                        name="document"
                        color="gray"
                        options={NATIONALITIES}
                        label="Nacionalidad"
                        onChange={this.handleChangeSelect("form")}
                      />
                    </div>
                    <div className="col-12 col-sm-12 col-md">
                      <Input
                        color="gray"
                        name="identification_id"
                        label="Identificación"
                        type="number"
                        onChange={this.handleChange("form")}
                      />
                    </div>
                    <div className="col-12 col-sm-12 col-md">
                      <DatePicker
                        label="Fecha Historia"
                        name="history"
                        onChange={(value) =>
                          this.handleChangeDatePicker(
                            "form",
                            "history",
                            value || new Date()
                          )
                        }
                        maxDate={today}
                        value={form.history}
                      />
                    </div>
                    <div className="col-12 col-sm-12 col-md">
                      <Input
                        color="gray"
                        name="surnames"
                        label="Apellidos"
                        type="text"
                        onChange={this.handleChange("form")}
                        maxlength={90}
                      />
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-12 col-sm-12 col-md">
                      <Input
                        color="gray"
                        name="names"
                        label="Nombres"
                        type="text"
                        onChange={this.handleChange("form")}
                        maxlength={90}
                      />
                    </div>
                    <div className="col-12 col-sm-12 col-md">
                      <DatePicker
                        label="Fecha Nacimiento"
                        name="birthday"
                        onChange={(value) =>
                          this.handleChangeBirthday(
                            "form",
                            "birthday",
                            value || new Date()
                          )
                        }
                        maxDate={todaySubtract}
                        value={form.birthday}
                      />
                    </div>
                    <div className="col-12 col-sm-12 col-md">
                      <Input
                        color="gray"
                        name="age"
                        label="Edad Cronológica"
                        type="number"
                        onChange={this.handleChange("form")}
                        maxlength={90}
                        value={form.age}
                      />
                    </div>
                    <div className="col-12 col-sm-12 col-md">
                      <Select
                        name="gender"
                        color="gray"
                        options={GENDER_SPORTY}
                        label="Género"
                        onChange={this.handleChangeSelect("form")}
                      />
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-12 col-sm-12 col-md">
                      <Input
                        color="gray"
                        name="birthplace"
                        label="Lugar Nacimiento"
                        type="text"
                        onChange={this.handleChange("form")}
                        maxlength={90}
                      />
                    </div>
                    <div className="col-12 col-sm-12 col-md">
                      <Select
                        name="phone_code"
                        color="gray"
                        options={CELLPHONE_CODE}
                        label="Código de Teléfono"
                        onChange={this.handleChangeSelect("form")}
                      />
                    </div>
                    <div className="col-12 col-sm-12 col-md">
                      <Input
                        color="gray"
                        name="phone"
                        label="Teléfono"
                        type="number"
                        onChange={this.handleChange("form")}
                      />
                    </div>
                    <div className="col-12 col-sm-12 col-md">
                      <Select
                        name="marital_status"
                        color="gray"
                        options={MARITAL_STATUS}
                        label="Edo. Civil"
                        onChange={this.handleChangeSelect("form")}
                      />
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                      <div className="form-group form-check">
                        <input
                          className="form-check-input"
                          type="checkbox"
                          id="statecity"
                          onChange={() =>
                            this.setState({ statecity: !statecity })
                          }
                        />
                        <label className="form-check-label" htmlFor="statecity">
                          Agregar Otro Estado y Ciudad
                        </label>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-12 col-sm-12 col-md">
                      <Input
                        color="gray"
                        name="occupation"
                        label="Profesión"
                        type="text"
                        onChange={this.handleChange("form")}
                        maxlength={90}
                      />
                    </div>
                    {statecity ? (
                      <React.Fragment>
                        <div className="col-12 col-sm-12 col-md">
                          <Input
                            color="gray"
                            name="state"
                            label="Estado"
                            type="text"
                            onChange={this.handleChange("form")}
                            maxlength={90}
                          />
                        </div>
                        <div className="col-12 col-sm-12 col-md">
                          <Input
                            color="gray"
                            name="city"
                            label="Ciudad"
                            type="text"
                            onChange={this.handleChange("form")}
                            maxlength={90}
                          />
                        </div>
                      </React.Fragment>
                    ) : (
                      <React.Fragment>
                        <div className="col-12 col-sm-12 col-md">
                          <Select
                            name="state_id"
                            color="gray"
                            options={state.map(({ id, name }) => ({
                              value: id,
                              label: name,
                            }))}
                            label="Estado"
                            onChange={this.handleChangeSelect("form")}
                          />
                        </div>
                        <div className="col-12 col-sm-12 col-md">
                          <Select
                            name="city_id"
                            color="gray"
                            options={city.map(({ id, name }) => ({
                              value: id,
                              label: name,
                            }))}
                            label="Ciudad"
                            onChange={this.handleChangeSelect("form")}
                          />
                        </div>
                      </React.Fragment>
                    )}
                    <div className="col-12 col-sm-12 col-md">
                      <Input
                        color="gray"
                        name="address"
                        label="Dirección"
                        type="text"
                        onChange={this.handleChange("form")}
                        maxlength={90}
                      />
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-12 col-sm-12 col-md">
                      <Input
                        color="gray"
                        name="email"
                        label="E-mail"
                        type="text"
                        onChange={this.handleChange("form")}
                        maxlength={90}
                      />
                    </div>
                    <div className="col-12 col-sm-12 col-md">
                      <Input
                        color="gray"
                        name="observations"
                        label="Observaciones"
                        type="text"
                        onChange={this.handleChange("form")}
                        maxlength={90}
                      />
                    </div>
                  </div>
                  <div className="text-center row">
                    <div className="col col-sm">
                      <Button
                        submitted={isSubmit}
                        block
                        type="submit"
                        className="btn-orange shadow"
                      >
                        Guardar
                      </Button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default HistoryCreate;
