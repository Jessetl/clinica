import React, { useState, useEffect } from "react";
import axios from "axios";
import { Card } from "react-bootstrap";
import { prop, handlerError } from "functions";

import { User } from "models";
import { ProfileService } from "services";
import { CELLPHONE_CODE, Globals } from "utils";
import { Input, Button, Select, Title } from "components";

import { useTypedSelector } from "reducers";
import { useTypedDispatch } from "store";

const Profile = () => {
  const [id, setId] = useState(0);
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [phoneCode, setPhoneCode] = useState("");
  const [phone, setPhone] = useState("");
  const [password, setPassword] = useState("");
  const [passwordConfirmation, setPasswordConfirmation] = useState("");

  const [submitted, setSubmitted] = useState(false);
  const user: User = useTypedSelector(prop("user"));
  const dispatch = useTypedDispatch();

  const cancelToken = axios.CancelToken;

  useEffect(() => {
    setId(user.id);
    setFirstName(user.person?.names || "");
    setLastName(user.person?.surnames || "");
    setEmail(user.email);
    setPhoneCode(user.person?.phone_code || "");
    setPhone(user.person?.phone || "");
  }, [user]);

  const submit = (event: any) => {
    event.preventDefault();

    const form = {
      id,
      names: firstName,
      surnames: lastName,
      phone_code: phoneCode,
      phone,
      password,
      password_confirmation: passwordConfirmation,
    };

    if (!password) {
      delete form.password;
      delete form.password_confirmation;
    }

    const source = cancelToken.source();

    setSubmitted(true);

    ProfileService.update(form, source)
      .then((user) => {
        dispatch({ user, type: "User/SET" });
        Globals.showSuccess(`¡Perfil actualizado correctamente!`);
      })
      .catch(handlerError)
      .finally(() => setSubmitted(false));
  };

  return (
    <div className="container">
      <div className="row">
        <div className="col col-md-8 offset-md-2">
          <Card className="border-0 shadow">
            <Title>Editar mi perfil</Title>
            <form
              onSubmit={submit}
              noValidate
              autoComplete="off"
              className="p-4"
            >
              <div className="row">
                <div className="col col-sm col-md">
                  <div className="form-group">
                    <p className="text-center mb-4 font-bold">
                      (Los campos con
                      <span className="text-orange"> * </span> son obligatorios)
                    </p>
                  </div>
                </div>
              </div>
              <div className="row ml-2">
                <div className="col col-sm col-md">
                  <Input
                    required
                    color="gray"
                    name="first_name"
                    label="Nombre"
                    maxlength={40}
                    onChange={(event: any) => {
                      setFirstName(event.target.value);
                    }}
                    value={firstName}
                  />
                </div>
                <div className="col col-sm col-md">
                  <Input
                    required
                    color="gray"
                    name="last_name"
                    label="Apellido"
                    maxlength={40}
                    onChange={(event: any) => {
                      setLastName(event.target.value);
                    }}
                    value={lastName}
                  />
                </div>
              </div>
              <div className="row ml-2">
                <div className="col col-sm col-md">
                  <Input
                    disabled
                    color="gray"
                    name="email"
                    label="E-mail"
                    maxlength={30}
                    onChange={(event: any) => {
                      setEmail(event.target.value);
                    }}
                    value={email}
                  />
                </div>
              </div>
              <div className="row ml-2">
                <div className="col col-sm col-md">
                  <Select
                    name="phone_code"
                    color="gray"
                    options={CELLPHONE_CODE}
                    label="Código de Teléfono"
                    onChange={(event: any) => {
                      setPhoneCode(event.target.value);
                    }}
                    valueSelect={phoneCode}
                  />
                </div>
                <div className="col col-sm col-md">
                  <Input
                    color="gray"
                    name="phone"
                    label="Teléfono"
                    maxlength={12}
                    onChange={(event: any) => {
                      setPhone(event.target.value);
                    }}
                    value={phone}
                  />
                </div>
              </div>
              <div className="row ml-2 mr-2 pr-1">
                <div className="col col-sm col-md">
                  <Input
                    color="gray"
                    type="password"
                    name="password"
                    label="Contraseña"
                    maxlength={20}
                    onChange={(event: any) => {
                      setPassword(event.target.value);
                    }}
                    value={password}
                  />
                </div>
                <div className="col col-sm">
                  <Input
                    color="gray"
                    type="password"
                    name="password_confirmation"
                    label="Confirmar contraseña"
                    maxlength={20}
                    onChange={(event: any) => {
                      setPasswordConfirmation(event.target.value);
                    }}
                    value={passwordConfirmation}
                  />
                </div>
              </div>
              <div className="text-center row ml-2">
                <div className="col col-sm">
                  <div className="form-group">
                    <Button
                      className="font-bold button-rounded shadow shadow-button px-5 font-size-md"
                      color="orange"
                      submitted={submitted}
                      type="submit"
                    >
                      GUARDAR CAMBIOS
                    </Button>
                  </div>
                </div>
              </div>
            </form>
          </Card>
        </div>
      </div>
    </div>
  );
};

export default Profile;
