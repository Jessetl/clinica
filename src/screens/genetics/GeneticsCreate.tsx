import React, { ReactNode } from "react";
import { RouteComponentProps } from "react-router-dom";
import axios, { CancelTokenSource } from "axios";

import { User, Form, TestTypeForm } from "models";
import { HistoryService, BoardService, FormService } from "services";

import { initialState, State } from "./FormState";
import { Input, Button, Select } from "components";
import { PageTitle, TitleModal } from "components";

import { handlerError, showSuccess, Globals, showError } from "utils";
import { FORM_METABOLIZER, FORM_GENETICS } from "utils";
import RoleId from "utils/RoleId";

import { RootState } from "reducers";
import { connect, ConnectedProps } from "react-redux";

interface MatchParams {
  id?: string;
}

const mapState = (state: RootState) => ({
  user: state.user,
  form: state.form,
});

const mapDispatch = {
  dispatchForm: (form: Form) => ({ form: form, type: "Form/SET" }),
};

const connector = connect(mapState, mapDispatch);

type Props = ConnectedProps<typeof connector> &
  RouteComponentProps<MatchParams>;

class GeneticsCreate extends React.PureComponent<Props, State> {
  public state: State;
  public source: CancelTokenSource;

  constructor(props: Props) {
    super(props);

    this.state = initialState;

    const cancelToken = axios.CancelToken;
    this.source = cancelToken.source();
  }

  componentDidMount() {
    const cancelToken = axios.CancelToken;
    this.source = cancelToken.source();

    this.getBoards();
    this.getUserHistoryData();
  }

  componentDidUpdate(_: Props, prevState: State) {
    if (
      prevState.boards !== this.state.boards &&
      this.state.boards.length > 0
    ) {
      this.getFormType();
    }
  }

  componentWillUnmount() {
    this.source.cancel("Cancel by user");
  }

  getBoards = async () => {
    try {
      const boards = await BoardService.boards(FORM_GENETICS, this.source);

      this.setState({ boards });
    } catch (err) {
      handlerError(err);
    }
  };

  getUserHistoryData = async () => {
    const { id } = this.props.match.params;

    if (!!id) {
      try {
        const doctor = await HistoryService.getById(parseInt(id), this.source);
        const { person } = doctor;

        if (!!person) {
          const parseHistory: User = {
            ...doctor,
            ...person,
            id: doctor.id,
          };

          console.log("history", parseHistory);

          this.setState({
            history: parseHistory,
            chronological: person.age,
          });
        }
      } catch (err) {
        handlerError(err);
      }
    }
  };

  getFormType = () => {
    const parseForm: TestTypeForm[] = [
      {
        name: "global_risk",
        translate: "Riesgo Global",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        name: "vascular_risk",
        translate: "Riesgo Vascular",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        name: "lipid_metabolism",
        translate: "Metabolismo Lipídico",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        name: "thrombosis",
        translate: "Trombosis",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        name: "hemorrhagic_stroke",
        translate: "Ictus Hemorrágico",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        name: "arterial_hypertension",
        translate: "Hipertensión arterial",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        name: "endothelial_vulnerability",
        translate: "Vulnerabilidad endotelial",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        name: "osteoporosis_risk",
        translate: "Riesgo de osteoporosis",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        name: "carcinogenic_risk",
        translate: "Riesgo Carcinogénico",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        name: "oxidative_stress",
        translate: "Estrés Oxidativo",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        name: "hemedinamics_adrener",
        translate: "Hemedinámica y Adrener",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        name: "potential_longevity",
        translate: "Potencial Longevidad",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        name: "metabolic_syndrome",
        translate: "Sindrome Metabólico",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        name: "environmental_tolerance",
        translate: "Tolerancia ambiental",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        name: "immune_response",
        translate: "Respuesta inmune",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        name: "affective_potential",
        translate: "Potencial psico afectivo",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
    ];

    this.setState({ form: parseForm });
  };

  handleChangeSelect = (key: string) => {
    return (event: React.ChangeEvent<HTMLSelectElement>): void => {
      const { value, name } = event.currentTarget;
      console.log("handleChangeSelect: key:", key, name);

      const parseValue = typeof value === "string" ? parseInt(value) : value;

      this.setState((prevState: any) => {
        return {
          ...prevState,
          [key]: parseValue || "",
        };
      });
    };
  };

  handleChange = (key: number, keyName?: string) => {
    return (event: React.ChangeEvent<HTMLInputElement>) => {
      const { value } = event.currentTarget;

      const copyForm = [...this.state.form];
      const parseValue = typeof value === "string" ? parseFloat(value) : value;
      const validValue = Number.isNaN(parseValue) ? "" : parseValue;

      console.log(parseValue, "parseValue");
      console.log(key, "key");

      copyForm[key] = {
        ...copyForm[key],
        relative_value: validValue,
      };

      this.setState((prevState: any) => {
        return {
          ...prevState,
          form: copyForm,
        };
      });
    };
  };

  getReverseByKey = (key: number): boolean => {
    switch (key) {
      default:
        return false;
    }
  };

  getAbsoluteResult = (
    name: string,
    value: number,
    reverse: boolean = false
  ): number => {
    const { boards } = this.state;

    if (!!boards && boards.length > 0) {
      const b = boards
        .filter((b) => b.name === name)
        .find((b) => Globals.getReverse(b, value, reverse));

      if (!!b) {
        const r = b.range; // D3 - C3
        const length = Math.abs(r.min - r.max); // F3

        if (!!r) {
          const absolute = Globals.getBoardSubtraction(b); // F4
          const calc = absolute / length; // F4 / F3

          console.log(absolute, "absolute");
          console.log(calc, "calc");

          let ageIn: number = parseFloat(b.min);
          let calcIn: number = 0;

          const agesRange = Globals.getRange(r.min, r.max);
          const ages = agesRange.map((pValue: number, key: number) => {
            calcIn = key > 0 ? calc + calcIn : calcIn;

            return {
              value: ageIn + calcIn,
              age: pValue,
            };
          });

          const reverseAges = reverse ? ages.reverse() : ages;
          const lastAge = reverseAges[reverseAges.length - 1];
          const higher = value > lastAge.value ? lastAge.age : 0;

          console.log(agesRange, "agesRange");
          console.log(ages, "ages");
          console.log(value, "value");

          return (
            reverseAges.find((a) => a.value > value || value <= a.value)?.age ||
            higher
          );
        }
      }
    }

    return 0;
  };

  handleSubmit = async (
    event: React.FormEvent<HTMLFormElement>
  ): Promise<void> => {
    const { user, match, dispatchForm } = this.props;
    const { history, chronological } = this.state;
    const { nat, cyp2d6, cyp2c19, cyp2c9 } = this.state;

    event.preventDefault();

    if (!!this.state.goToBack && match.params.id) {
      return this.props.history.replace(
        `/admin/stories/${match.params.id}/edit`
      );
    }

    if (!!!this.state.isSubmit) {
      let forms = [...this.state.form];

      const testForm = forms.find(
        (f) => !!!f.relative_value && f.relative_value !== 0
      );

      if (!!testForm) {
        return showError(`${testForm.translate} es requerido para el calculo.`);
      } else if (!!!nat) {
        return showError(`Metabolizador NAT es requerido.`);
      } else if (!!!cyp2d6) {
        return showError(`Metabolizador CYP2D6 es requerido.`);
      } else if (!!!cyp2c19) {
        return showError(`Metabolizador CYP2C19 es requerido.`);
      } else if (!!!cyp2c9) {
        return showError(`Metabolizador CYPE2C9 es requerido.`);
      }

      for (let i = 0; i < forms.length; i++) {
        forms[i].absolute_value = this.getAbsoluteResult(
          forms[i].name,
          forms[i].relative_value || 0,
          this.getReverseByKey(i)
        );
      }

      if (!!user) {
        const genetics = parseInt(
          (
            forms.reduce(function (pValue: number, cValue: TestTypeForm) {
              return pValue + getTypeOfValue(cValue.absolute_value || 0);
            }, 0) / 16
          ).toFixed(0)
        );

        const differential = Math.abs(chronological - genetics) * 1;

        this.setState({
          isSubmit: true,
          genetics: genetics,
          differential: differential,
          form: forms,
        });

        FormService.storeGenetics(
          forms,
          {
            userId: history.id,
            chronological: chronological,
            genetics: genetics,
            differential: differential,
            nat,
            cyp2d6,
            cyp2c19,
            cyp2c9,
          },
          this.source
        )
          .then((user: User) => {
            showSuccess();

            this.setState({ goToBack: true });
            dispatchForm({ remaining: user.forms });
          })
          .catch(handlerError)
          .finally(() => {
            this.setState({ isSubmit: false });
          });
      }
    }
  };

  render(): ReactNode {
    const { user, match, form: reduxForm } = this.props;
    const { isSubmit, form, goToBack } = this.state;
    const { chronological, genetics, differential } = this.state;

    return (
      <div className="container">
        <PageTitle backUrl={`/admin/stories/${match.params.id}/edit`}>
          Registro de Test
        </PageTitle>
        <div className="row justify-content-center">
          <div className="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
            <TitleModal>Test Edad Genética</TitleModal>
            {!!user && user.role !== RoleId.ROOT && (
              <p>Cantidad de usos disponibles: {reduxForm?.remaining}</p>
            )}
            <div className="card">
              <div className="card-body">
                <form
                  onSubmit={this.handleSubmit}
                  noValidate
                  autoComplete="off"
                >
                  {!!form && form.length > 0 && (
                    <React.Fragment>
                      {form.map((row, key) => {
                        return (
                          <div className="row" key={key.toString()}>
                            <div className="col-12 col-sm-12 col-md">
                              <label htmlFor={row.name}>{row.translate}</label>
                              <div className="form-group">
                                <Input
                                  name={row.name}
                                  color="gray"
                                  type="number"
                                  onChange={this.handleChange(key)}
                                  step="0.001"
                                  value={row.relative_value}
                                />
                              </div>
                            </div>
                            <div className="col-12 col-sm-12 col-md">
                              <div className="form-group">
                                <Input
                                  disabled
                                  labelClass="invisible"
                                  label="Resultado"
                                  color="gray"
                                  type="text"
                                  value={row.absolute_value}
                                />
                              </div>
                            </div>
                          </div>
                        );
                      })}
                    </React.Fragment>
                  )}

                  <div className="row">
                    <div className="col-12 col-sm-12 col-md">
                      <div className="form-group">
                        <Input
                          name="chronological"
                          label="Edad Cronológica"
                          color="gray"
                          type="text"
                          onChange={(
                            event: React.ChangeEvent<HTMLInputElement>
                          ): void => {
                            const { value, name } = event.currentTarget;
                            const parseValue =
                              typeof value === "string"
                                ? parseInt(value)
                                : value;

                            this.setState((prevState: any) => {
                              return {
                                ...prevState,
                                [name]: parseValue || "",
                              };
                            });
                          }}
                          value={chronological}
                        />
                      </div>
                    </div>
                    <div className="col-12 col-sm-12 col-md">
                      <div className="form-group">
                        <Input
                          disabled
                          label="Edad Genética"
                          color="gray"
                          type="text"
                          value={genetics}
                        />
                      </div>
                    </div>
                    <div className="col-12 col-sm-12 col-md">
                      <div className="form-group">
                        <Input
                          disabled
                          label="Edad Diferencial"
                          color="gray"
                          type="text"
                          value={differential}
                        />
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-12 col-sm-12 col-md">
                      <Select
                        name="nat"
                        color="gray"
                        options={FORM_METABOLIZER}
                        label="NAT"
                        onChange={this.handleChangeSelect("nat")}
                        valueSelect={this.state.nat}
                      />
                    </div>
                    <div className="col-12 col-sm-12 col-md">
                      <Select
                        name="cyp2d6"
                        color="gray"
                        options={FORM_METABOLIZER}
                        label="CYP2D6"
                        onChange={this.handleChangeSelect("cyp2d6")}
                        valueSelect={this.state.cyp2d6}
                      />
                    </div>
                    <div className="col-12 col-sm-12 col-md">
                      <Select
                        name="cyp2c19"
                        color="gray"
                        options={FORM_METABOLIZER}
                        label="CYP2C19"
                        onChange={this.handleChangeSelect("cyp2c19")}
                        valueSelect={this.state.cyp2c19}
                      />
                    </div>
                    <div className="col-12 col-sm-12 col-md">
                      <Select
                        name="cyp2c9"
                        color="gray"
                        options={FORM_METABOLIZER}
                        label="CYP2C9"
                        onChange={this.handleChangeSelect("cyp2c9")}
                        valueSelect={this.state.cyp2c9}
                      />
                    </div>
                  </div>
                  <div className="text-center row">
                    <div className="col col-sm">
                      <Button
                        submitted={isSubmit}
                        block
                        type="submit"
                        className="btn-orange shadow"
                      >
                        {!!goToBack ? "Salir" : "Calcular y Guardar"}
                      </Button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function getTypeOfValue(cValue: number): number {
  return typeof cValue === "string" ? parseInt(cValue) : cValue;
}

export default connector(GeneticsCreate);
