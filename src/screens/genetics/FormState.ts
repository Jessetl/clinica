import { User, TestTypeForm, Board } from "models";

export type State = {
  isSubmit: boolean;
  error: string;
  boards: Board[];
  form: TestTypeForm[];
  formType: number | "";
  history: User;
  chronological: number;
  genetics: number;
  differential: number;
  nat: number | "";
  cyp2d6: number | "";
  cyp2c19: number | "";
  cyp2c9: number | "";
  goToBack: boolean;
};

export const initialState: State = {
  isSubmit: false,
  error: "",
  boards: [],
  form: [],
  formType: "",
  history: {
    id: 0,
    alphanumeric: "",
    user_id: 0,
    role: 0,
    email: "",
    coach: 0,
    forms: 0,
    status: 0,
    activated: 0,
    remember_token: "",
    created_at: new Date(),
    updated_at: new Date(),
    deleted_at: new Date(),
  },
  chronological: 0,
  genetics: 0,
  differential: 0,
  nat: "",
  cyp2d6: "",
  cyp2c19: "",
  cyp2c9: "",
  goToBack: false,
};
