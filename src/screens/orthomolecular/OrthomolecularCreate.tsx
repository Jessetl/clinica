import React, { ReactNode } from "react";
import { RouteComponentProps } from "react-router-dom";
import axios, { CancelTokenSource } from "axios";

import { User, Form, TestTypeForm, Age } from "models";
import { HistoryService, BoardService, FormService } from "services";

import { initialState, State } from "./FormState";
import { Input, Button } from "components";
import { PageTitle, TitleModal } from "components";

import { handlerError, showSuccess, Globals, showError } from "utils";
import { FORM_ORTHOMOLECULAR } from "utils";
import RoleId from "utils/RoleId";

import { RootState } from "reducers";
import { connect, ConnectedProps } from "react-redux";

interface MatchParams {
  id?: string;
}

const mapState = (state: RootState) => ({
  user: state.user,
  form: state.form,
});

const mapDispatch = {
  dispatchForm: (form: Form) => ({ form: form, type: "Form/SET" }),
};

const connector = connect(mapState, mapDispatch);

type Props = ConnectedProps<typeof connector> &
  RouteComponentProps<MatchParams>;

class OrthomolecularCreate extends React.PureComponent<Props, State> {
  public state: State;
  public source: CancelTokenSource;

  constructor(props: Props) {
    super(props);

    this.state = initialState;

    const cancelToken = axios.CancelToken;
    this.source = cancelToken.source();
  }

  componentDidMount() {
    const cancelToken = axios.CancelToken;
    this.source = cancelToken.source();

    this.getBoards();
    this.getUserHistoryData();
  }

  componentDidUpdate(_: Props, prevState: State) {
    if (
      prevState.boards !== this.state.boards &&
      this.state.boards.length > 0
    ) {
      this.getFormType();
    }
  }

  componentWillUnmount() {
    this.source.cancel("Cancel by user");
  }

  getBoards = async () => {
    try {
      const boards = await BoardService.boards(
        FORM_ORTHOMOLECULAR,
        this.source
      );

      this.setState({ boards });
    } catch (err) {
      handlerError(err);
    }
  };

  getUserHistoryData = async () => {
    const { id } = this.props.match.params;

    if (!!id) {
      try {
        const doctor = await HistoryService.getById(parseInt(id), this.source);
        const { person } = doctor;

        if (!!person) {
          const parseHistory: User = {
            ...doctor,
            ...person,
            id: doctor.id,
          };

          console.log("history", parseHistory);

          this.setState({
            history: parseHistory,
            chronological: person.age,
          });
        }
      } catch (err) {
        handlerError(err);
      }
    }
  };

  getFormType = () => {
    const parseForm: TestTypeForm[] = [
      {
        // 1
        name: "aluminum",
        translate: "Aluminio",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 2
        name: "antimony",
        translate: "Antimonio",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 3
        name: "arsenic",
        translate: "Arsénico",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 4
        name: "barium",
        translate: "Bario",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 5
        name: "beryllium",
        translate: "Berilio",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 6
        name: "bismuth",
        translate: "Bismuto",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 7
        name: "cadmium",
        translate: "Cadmio",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 8
        name: "mercury",
        translate: "Mercurio",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 9
        name: "nickel",
        translate: "Niquel",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 10
        name: "silver",
        translate: "Plata",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 11
        name: "platinum",
        translate: "Platino",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 12
        name: "lead",
        translate: "Plomo",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 13
        name: "thallium",
        translate: "Talio",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 14
        name: "tinio",
        translate: "Tinio",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 15
        name: "titanium",
        translate: "Titanio",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 16
        name: "thorium",
        translate: "Torio",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 17
        name: "uranium",
        translate: "Uranio",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 18
        name: "calcium",
        translate: "Calcio",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 19
        name: "magnesium",
        translate: "Magnesio",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 20
        name: "sodium",
        translate: "Sodio",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 21
        name: "potassium",
        translate: "Potasio",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 22
        name: "copper",
        translate: "Cobre",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 23
        name: "zinc",
        translate: "Zinc",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 24
        name: "manganese",
        translate: "Manganeso",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 25
        name: "chrome",
        translate: "Cromo",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 26
        name: "vanadium",
        translate: "Vanadio",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 27
        name: "molybdenum",
        translate: "Molibdeno",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 28
        name: "boron",
        translate: "Boro",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 29
        name: "iodine",
        translate: "Yodo",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 30
        name: "lithium",
        translate: "Litio",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 31
        name: "phosphoro",
        translate: "Phosphoro",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 32
        name: "selenium",
        translate: "Selenio",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 33
        name: "stronium",
        translate: "Estronio",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 34
        name: "sulfur",
        translate: "Azufre",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 35
        name: "cobalt",
        translate: "Cobalto",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 36
        name: "iron",
        translate: "Hierro",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 37
        name: "germanium",
        translate: "Germanio",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 38
        name: "rubidium",
        translate: "Rubidio",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
      {
        // 39
        name: "zirconium",
        translate: "Zirconio",
        dimensions: false,
        relative_value: "",
        absolute_value: "",
      },
    ];

    this.setState({ form: parseForm });
  };

  handleChangeSelect = (key: string) => {
    return (event: React.ChangeEvent<HTMLSelectElement>): void => {
      const { value, name } = event.currentTarget;
      console.log("handleChangeSelect: key:", key, name);

      const parseValue = typeof value === "string" ? parseInt(value) : value;

      this.setState((prevState: any) => {
        return {
          ...prevState,
          [key]: parseValue || 0,
          form: [],
        };
      });
    };
  };

  handleChange = (key: number, keyName?: string) => {
    return (event: React.ChangeEvent<HTMLInputElement>) => {
      const { value } = event.currentTarget;

      const copyForm = [...this.state.form];
      const parseValue = typeof value === "string" ? parseFloat(value) : value;
      const validValue = Number.isNaN(parseValue) ? "" : parseValue;

      console.log(parseValue, "parseValue");
      console.log(key, "key");

      copyForm[key] = {
        ...copyForm[key],
        relative_value: validValue,
      };

      this.setState((prevState: any) => {
        return {
          ...prevState,
          form: copyForm,
        };
      });
    };
  };

  getAbsoluteResult = (
    name: string,
    value: number,
    reverse: boolean = false
  ): number => {
    const { boards } = this.state;

    if (!!boards && boards.length > 0) {
      let b = boards
        .filter((b) => b.name === name)
        .find((b) => Globals.getReverse(b, value, reverse));

      if (!!b) {
        const r = b.range; // D3 - C3
        const length = Math.abs(r.min - r.max); // F3

        if (!!r) {
          const absolute = Globals.getBoardSubtraction(b); // F4
          const calc = absolute / length; // F4 / F3

          console.log(absolute, "absolute");
          console.log(calc, "calc");

          let ageIn: number = parseFloat(b.min);
          let calcIn: number = 0;

          const agesRange = Globals.getRange(r.min, r.max);
          const ages: Age[] = agesRange.map((pValue: number, key: number) => {
            calcIn = key > 0 ? calc + calcIn : calcIn;

            return {
              value: ageIn + calcIn,
              age: pValue,
            };
          });

          const reverseAges = reverse ? ages.reverse() : ages;
          const lastAge = reverseAges[reverseAges.length - 1];
          const higher = value > lastAge.value ? lastAge.age : 0;

          console.log(agesRange, "agesRange");
          console.log(ages, "ages");
          console.log(value, "value");

          return (
            reverseAges.find((a) => a.value > value || value <= a.value)?.age ||
            higher
          );
        }
      }
    }

    return 0;
  };

  handleSubmit = async (
    event: React.FormEvent<HTMLFormElement>
  ): Promise<void> => {
    const { user, match, dispatchForm } = this.props;
    const { history, chronological } = this.state;

    event.preventDefault();

    if (!!this.state.goToBack && match.params.id) {
      return this.props.history.replace(
        `/admin/stories/${match.params.id}/edit`
      );
    }

    if (!!!this.state.isSubmit) {
      let forms = [...this.state.form];
      const testForm = forms.find(
        (f) => !!!f.relative_value && f.relative_value !== 0
      );

      if (!!testForm) {
        return showError(`${testForm.translate} es requerido para el calculo.`);
      }

      for (let i = 0; i < forms.length; i++) {
        forms[i].absolute_value = this.getAbsoluteResult(
          forms[i].name,
          forms[i].relative_value || 0,
          false
        );
      }

      if (!!user) {
        const orthomolecular = parseInt(
          (
            forms.reduce(function (pValue: number, cValue: TestTypeForm) {
              return pValue + getTypeOfValue(cValue.absolute_value || 0);
            }, 0) / 39
          ).toFixed(0)
        );

        const differential = Math.abs(chronological - orthomolecular) * 1;

        this.setState({
          isSubmit: true,
          orthomolecular: orthomolecular,
          differential: differential,
          form: forms,
        });

        FormService.storeOrthomolecular(
          forms,
          {
            userId: history.id,
            chronological: chronological,
            orthomolecular: orthomolecular,
            differential: differential,
          },
          this.source
        )
          .then((user: User) => {
            showSuccess();

            this.setState({ goToBack: true });
            dispatchForm({ remaining: user.forms });
          })
          .catch(handlerError)
          .finally(() => this.setState({ isSubmit: false }));
      }
    }
  };

  render(): ReactNode {
    const { user, match, form: reduxForm } = this.props;
    const { isSubmit, form, goToBack } = this.state;
    const { chronological, orthomolecular, differential } = this.state;

    return (
      <div className="container">
        <PageTitle backUrl={`/admin/stories/${match.params.id}/edit`}>
          Registro de Test
        </PageTitle>
        <div className="row justify-content-center">
          <div className="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
            <TitleModal>Test Edad Ortomolecular</TitleModal>
            {!!user && user.role !== RoleId.ROOT && (
              <p>Cantidad de usos disponibles: {reduxForm?.remaining}</p>
            )}
            <div className="card">
              <div className="card-body">
                <form
                  onSubmit={this.handleSubmit}
                  noValidate
                  autoComplete="off"
                >
                  {!!form && form.length > 0 && (
                    <React.Fragment>
                      {form.map((row, key) => {
                        return (
                          <div className="row" key={key.toString()}>
                            <div className="col-12 col-sm-12 col-md">
                              <label htmlFor={row.name}>{row.translate}</label>
                              <div className="form-group">
                                <Input
                                  name={row.name}
                                  color="gray"
                                  type="number"
                                  onChange={this.handleChange(key)}
                                  value={row.relative_value}
                                  step="0.001"
                                />
                              </div>
                            </div>
                            <div className="col-12 col-sm-12 col-md">
                              <div className="form-group">
                                <Input
                                  disabled
                                  labelClass="invisible"
                                  label="Resultado"
                                  color="gray"
                                  type="text"
                                  value={row.absolute_value}
                                />
                              </div>
                            </div>
                          </div>
                        );
                      })}
                    </React.Fragment>
                  )}

                  <div className="row">
                    <div className="col-12 col-sm-12 col-md">
                      <div className="form-group">
                        <Input
                          name="chronological"
                          label="Edad Cronológica"
                          color="gray"
                          type="text"
                          onChange={(
                            event: React.ChangeEvent<HTMLInputElement>
                          ): void => {
                            const { value, name } = event.currentTarget;
                            const parseValue =
                              typeof value === "string"
                                ? parseInt(value)
                                : value;

                            this.setState((prevState: any) => {
                              return {
                                ...prevState,
                                [name]: parseValue || "",
                              };
                            });
                          }}
                          value={chronological}
                        />
                      </div>
                    </div>
                    <div className="col-12 col-sm-12 col-md">
                      <div className="form-group">
                        <Input
                          disabled
                          label="Edad Orthomolecular"
                          color="gray"
                          type="text"
                          value={orthomolecular}
                        />
                      </div>
                    </div>
                    <div className="col-12 col-sm-12 col-md">
                      <div className="form-group">
                        <Input
                          disabled
                          label="Edad Diferencial"
                          color="gray"
                          type="text"
                          value={differential}
                        />
                      </div>
                    </div>
                  </div>

                  <div className="text-center row">
                    <div className="col col-sm">
                      <Button
                        submitted={isSubmit}
                        block
                        type="submit"
                        className="btn-orange shadow"
                      >
                        {!!goToBack ? "Salir" : "Calcular y Guardar"}
                      </Button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function getTypeOfValue(cValue: number): number {
  return typeof cValue === "string" ? parseInt(cValue) : cValue;
}

export default connector(OrthomolecularCreate);
