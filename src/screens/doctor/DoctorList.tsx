import React, { useState, useEffect, FunctionComponent } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";

import { User } from "models";
import { DoctorService } from "services";

import { UserProps } from "./FormState";
import { Loading, Pagination, Empty, Button, Input, Icon } from "components";
import { confirm, handlerError } from "utils";
import { STATUS_ACTIVE, DOCTOR_COACH, FILE_TYPES } from "utils";

import eyeSvg from "assets/icons/eye.svg";
import deleteSvg from "assets/icons/delete.svg";

export default function DoctorList() {
  const history = useHistory();
  const [users, setUsers] = useState<User[]>([]);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [page, setPage] = useState<number>(1);
  const [lastPage, setLastPage] = useState<number>(1);
  const [query, setQuery] = useState<string>("");

  const cancelToken = axios.CancelToken;

  useEffect(() => {
    const source = cancelToken.source();

    if (!!!query) {
      (async () => {
        try {
          const { data, last_page } = await DoctorService.doctorsByPage(
            page,
            source
          );

          setIsLoading(false);
          setLastPage(last_page);
          setUsers(data);
        } catch (err) {
          handlerError(err);
        }

        return () => source.cancel("Cancel by user");
      })();
    }
  }, [page, cancelToken]);

  const onChangePage = async (page: number) => {
    const source = cancelToken.source();
    setIsLoading(true);

    try {
      const { data, last_page } = await DoctorService.doctorsByPage(
        page,
        source
      );

      setIsLoading(false);
      setPage(page);
      setLastPage(last_page);
      setUsers(data);
    } catch (err) {
      handlerError(err);
    }
  };

  const onDelete = async (doctorId: number) => {
    const hasDelete = await confirm({ text: "¿Desea eliminar este doctor?" });

    if (!hasDelete) {
      return;
    }

    await DoctorService.destroy(doctorId);
    setUsers(users.filter(({ id }) => id !== doctorId));
  };

  const toCreate = () => history.push("/admin/doctors/create");

  const toEdit = (id: number) => history.push(`/admin/doctors/${id}/edit`);

  const toForms = (id: number) => history.push(`/admin/doctors/${id}/forms`);

  const toClean = async () => {
    setQuery("");
    toQuerySearch(true);
  };

  const toExport = async (typeFile: string) => {
    // @ts-ignore
    const extension = FILE_TYPES[typeFile].extension;
    // @ts-ignore
    const type = FILE_TYPES[typeFile].type;

    const data = await DoctorService.export({ query }, typeFile);

    const url = window.URL.createObjectURL(
      new Blob([data], {
        type,
      })
    );
    const link = document.createElement("a");

    const filename = "REPORTE_DE_DOCTORES" + extension;

    link.href = url;
    link.setAttribute("download", `${filename}`);

    document.body.appendChild(link);
    link.click();
  };

  const onChangeStatus = async (userId: number) => {
    const hasStatus = await confirm({
      text: "¿Desea cambiar el estatus de este doctor?",
    });

    if (!!hasStatus) {
      const source = cancelToken.source();

      try {
        const user = await DoctorService.status(userId, source);
        const userKey = users.findIndex(({ id }) => id === userId);

        let data = [...users];

        if (!!data[userKey]) {
          data[userKey] = {
            ...user,
          };
        }

        setUsers(data);
      } catch (err) {
        handlerError(err);
      }
    }
  };

  const toQuerySearch = async (clean?: boolean) => {
    const source = cancelToken.source();
    setIsLoading(true);

    const queryReset = !!clean ? "" : query;

    try {
      const { data, last_page } = await DoctorService.queryDoctorsByPage(
        1,
        queryReset,
        source
      );

      setIsLoading(false);
      setPage(1);
      setLastPage(last_page);
      setUsers(data);
    } catch (err) {
      handlerError(err);
    }
  };

  if (isLoading) {
    return <Loading />;
  }

  return (
    <div className="container">
      <div className="row mb-3">
        <h1>Profesionales</h1>
      </div>
      <div className="row">
        <div className="col-md">
          <Button
            color="orange"
            title="Crear"
            className="float-right button-rounded mx-2 my-2 btn-lg d-inline size-large"
            onClick={(event: any) => {
              event.preventDefault();
              toQuerySearch();
            }}
          >
            Buscar
          </Button>
          <Button
            color="orange"
            title="Crear"
            className="float-right button-rounded mx-2 my-2 btn-lg d-inline size-large"
            onClick={toCreate}
          >
            Crear Profesional
          </Button>
        </div>
      </div>
      <div className="row">
        <div className="col-md">
          <Input
            name="query"
            maxlength={30}
            placeholder="Filtro: Nombre, Apellido, Cédula"
            onChange={(event: any) => {
              setQuery(event.target.value);
            }}
            onKeyPress={(event: any) => {
              if (event.key === "Enter") {
                toQuerySearch();
              }
            }}
            value={query}
          />
        </div>
        <div className="col-md mb-3">
          <Button
            color="orange"
            title="Crear"
            className="float-right button-rounded mx-2 my-1 btn-lg d-inline size-large"
            onClick={() => toExport("excel")}
          >
            Exportar Excel
          </Button>
          <Button
            color="orange"
            title="Crear"
            className="float-right button-rounded mx-2 my-1 btn-lg d-inline size-large"
            onClick={toClean}
          >
            Limpiar Filtro
          </Button>
        </div>
      </div>

      <div className="row rounded-lg shadow-sm bg-white mb-3">
        <div className="col-md-2 px-2 py-2 rounded-left font-bold border border-right-0 d-flex align-items-center">
          Nombre y Apellido
        </div>
        <div className="col-md-1 px-2 py-2 font-bold border border-right-0 d-flex align-items-center">
          Cédula
        </div>
        <div className="col-md-2 px-2 py-2 font-bold border border-right-0 d-flex align-items-center">
          Correo
        </div>
        <div className="col-md-1 px-2 py-2 font-bold border border-right-0 d-flex align-items-center">
          Coach
        </div>
        <div className="col-md-1 px-2 py-2 font-bold border border-right-0 d-flex align-items-center">
          Estatus
        </div>
        <div className="col-md-1 px-2 py-2 font-bold border border-right-0 d-flex align-items-center">
          Formularios
        </div>
        <div className="col-md-4 px-2 py-2 text-center border border-right-0 d-flex align-items-center justify-content-center">
          Acciones
        </div>
      </div>

      {users.map((user) => (
        <RowUser
          key={user.id}
          user={user}
          onEdit={(id) => toEdit(id)}
          onChangeStatus={(id) => onChangeStatus(id)}
          toForms={toForms}
          onDelete={onDelete}
        />
      ))}

      {users.length === 0 && <Empty />}

      <div className="row my-3">
        <div className="col-md">
          <Pagination pages={lastPage} active={page} onChange={onChangePage} />
        </div>
      </div>
    </div>
  );
}

const RowUser: FunctionComponent<UserProps> = (props) => {
  const { user, onEdit, onChangeStatus, toForms, onDelete } = props;

  return (
    <div className="row rounded-lg shadow-sm bg-white mb-3">
      <div className="col-md-2 px-2 py-2 bg-orange rounded-left font-bold d-flex align-items-center">
        {user.person?.full_name}
      </div>
      <div className="col-md-1 px-2 py-2 bg-orange font-bold d-flex align-items-center">
        {user.person?.full_identification}
      </div>
      <div className="col-md-2 px-2 py-2 bg-orange font-bold d-flex align-items-center">
        {user.email}
      </div>
      <div className="col-md-1 px-2 py-2 bg-orange font-bold d-flex align-items-center">
        {user.coach === DOCTOR_COACH ? "Sí" : "No"}
      </div>
      <div className="col-md-1 px-2 py-2 bg-orange font-bold d-flex align-items-center">
        {user.status === STATUS_ACTIVE ? "Activo" : "Inactivo"}
      </div>
      <div className="col-md-1 px-2 py-2 bg-orange font-bold d-flex align-items-center">
        {user.forms}
      </div>
      <div className="col-md-1 px-2 py-2 text-center border border-right-0 d-flex align-items-center justify-content-center">
        <Button
          title={user.status === STATUS_ACTIVE ? "Desactivar" : "Activar"}
          small
          onClick={(event: React.FormEvent<HTMLButtonElement>): void => {
            event.preventDefault();
            onChangeStatus(user.id);
          }}
        >
          <Icon
            name={
              user.status === STATUS_ACTIVE
                ? "toggle-on fa-2x mt-1"
                : "toggle-off fa-2x mt-1"
            }
          />
        </Button>
      </div>
      <div className="col-md-1 px-2 py-2 text-center border border-right-0 d-flex align-items-center justify-content-center">
        <Button
          title="Formularios"
          small
          onClick={(event: React.FormEvent<HTMLButtonElement>): void => {
            event.preventDefault();
            toForms(user.id);
          }}
        >
          <Icon name="wpforms fa-2x mt-1" />
        </Button>
      </div>
      <div className="col-md-1 px-2 py-2 text-center border border-right-0 d-flex align-items-center justify-content-center">
        <Button
          color="orange"
          title="Ver ó Editar"
          small
          className="rounded rounded-circle"
          onClick={(event: React.FormEvent<HTMLButtonElement>): void => {
            event.preventDefault();
            onEdit(user.id);
          }}
        >
          <img src={eyeSvg} alt="Ver ó Editar" />
        </Button>
      </div>
      <div className="col-md-1 px-2 py-2 text-center border border-right-0 d-flex align-items-center justify-content-center">
        <Button
          color="dark"
          title="Eliminar"
          small
          className="rounded rounded-circle"
          onClick={(event: React.FormEvent<HTMLButtonElement>): void => {
            event.preventDefault();
            onDelete(user.id);
          }}
        >
          <img src={deleteSvg} alt="eliminar" />
        </Button>
      </div>
    </div>
  );
};
