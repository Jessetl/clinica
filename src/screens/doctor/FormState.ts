import { DoctorForm, State as States, City, User } from "models";
import moment from "moment";

export type State = {
  isSubmit: boolean;
  error: string;
  state: States[];
  city: City[];
  form: DoctorForm;
};

export type UserProps = {
  user: User;
  onEdit: (id: number) => void;
  onChangeStatus: (id: number) => void;
  onDelete: (id: number) => void;
  toForms: (id: number) => void;
};

export const today = moment().toDate();
export const todaySubtract = moment().subtract(18, "year").toDate();

export const initialState: State = {
  isSubmit: false,
  error: "",
  state: [],
  city: [],
  form: {
    id: "",
    user_id: "",
    document: "",
    identification_id: "",
    surnames: "",
    names: "",
    birthday: todaySubtract,
    phone_code: "",
    phone: "",
    coach: 0,
    status: 1,
    email: "",
    password: "",
    password_confirmation: "",
  },
};
