import React, { ReactNode } from "react";
import { RouteComponentProps } from "react-router-dom";
import axios, { CancelTokenSource } from "axios";

import { DoctorForm } from "models";
import { DoctorService } from "services";

import { initialState, State } from "./FormState";
import { Select, Input, DatePicker, Button } from "components";
import { PageTitle, TitleModal } from "components";
import { NATIONALITIES, CELLPHONE_CODE } from "utils";
import { BOOLEAN_STATUS, STATUS_USER } from "utils";
import { handlerError, showSuccess } from "utils";
import { todaySubtract } from "screens/history/FormState";

interface MatchParams {
  id?: string;
}

type Props = RouteComponentProps<MatchParams>;

class DoctorEdit extends React.PureComponent<Props, State> {
  public state: State;
  public source: CancelTokenSource;

  constructor(props: Props) {
    super(props);

    this.state = initialState;

    const cancelToken = axios.CancelToken;
    this.source = cancelToken.source();
  }

  componentDidMount() {
    const cancelToken = axios.CancelToken;
    this.source = cancelToken.source();

    this.getDoctorData();
  }

  componentWillUnmount() {
    this.source.cancel("Cancel by user");
  }

  getDoctorData = async () => {
    const { id } = this.props.match.params;

    if (!!id) {
      try {
        const doctor = await DoctorService.getById(parseInt(id), this.source);
        const { person } = doctor;

        if (!!person) {
          const parseDoctor: DoctorForm = {
            ...doctor,
            ...person,
            id: doctor.id,
            birthday: new Date(Date.parse(person.birthday.toString())),
            password: "",
            password_confirmation: "",
          };

          console.log("doctor", parseDoctor);

          this.setState({
            form: parseDoctor,
          });
        }
      } catch (err) {
        handlerError(err);
      }
    }
  };

  handleChangeSelect = (key: string) => {
    return (event: React.ChangeEvent<HTMLSelectElement>): void => {
      const { value, name } = event.currentTarget;
      console.log("handleChangeSelect: key:", key, name);

      this.setState((prevState: any) => {
        return {
          ...prevState,
          [key]: {
            ...prevState[key],
            [name]: value,
          },
        };
      });
    };
  };

  handleChange = (key: string) => {
    return (event: React.ChangeEvent<HTMLInputElement>) => {
      const { value, name } = event.currentTarget;
      this.setState((prevState: any) => {
        return {
          ...prevState,
          [key]: {
            ...prevState[key],
            [name]: value,
          },
        };
      });
    };
  };

  handleChangeDatePicker = (key: string, value: Date) => {
    this.setState((prevState: any) => {
      return {
        ...prevState,
        [key]: {
          ...prevState[key],
          birthday: value,
        },
      };
    });
  };

  handleSubmit = async (
    event: React.FormEvent<HTMLFormElement>
  ): Promise<void> => {
    event.preventDefault();

    if (this.state.isSubmit) {
      return;
    }

    this.setState({
      isSubmit: true,
    });

    const form: DoctorForm = {
      ...this.state.form,
    };

    if (!form.password) {
      delete form.password;
      delete form.password_confirmation;
    }

    console.log("handleSubmit form: ", form);

    DoctorService.update(form, this.source)
      .then(() => {
        showSuccess();

        setTimeout(() => {
          this.props.history.push("/admin/doctors");
        }, 1000);
      })
      .catch(handlerError)
      .finally(() => this.setState({ isSubmit: false }));
  };

  render(): ReactNode {
    const { isSubmit, form } = this.state;

    return (
      <div className="container">
        <PageTitle backUrl="/admin/doctors">Edición de Profecional</PageTitle>
        <div className="row justify-content-center">
          <div className="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
            <TitleModal>Editar Profesional</TitleModal>
            <div className="card">
              <div className="card-body">
                <form
                  onSubmit={this.handleSubmit}
                  noValidate
                  autoComplete="off"
                >
                  <div className="row">
                    <div className="col-12 col-sm-12 col-md">
                      <Select
                        name="document"
                        color="gray"
                        options={NATIONALITIES}
                        label="Nacionalidad"
                        onChange={this.handleChangeSelect("form")}
                        valueSelect={form.document}
                      />
                    </div>
                    <div className="col-12 col-sm-12 col-md">
                      <Input
                        color="gray"
                        name="identification_id"
                        label="Identificación"
                        type="number"
                        onChange={this.handleChange("form")}
                        value={form.identification_id}
                      />
                    </div>
                    <div className="col-12 col-sm-12 col-md">
                      <Input
                        color="gray"
                        name="surnames"
                        label="Apellidos"
                        type="text"
                        onChange={this.handleChange("form")}
                        maxlength={90}
                        value={form.surnames}
                      />
                    </div>
                    <div className="col-12 col-sm-12 col-md">
                      <Input
                        color="gray"
                        name="names"
                        label="Nombres"
                        type="text"
                        onChange={this.handleChange("form")}
                        maxlength={90}
                        value={form.names}
                      />
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-12 col-sm-12 col-md">
                      <DatePicker
                        label="Fecha Nacimiento"
                        name="birthday"
                        onChange={(value) =>
                          this.handleChangeDatePicker(
                            "form",
                            value || new Date()
                          )
                        }
                        maxDate={todaySubtract}
                        value={form.birthday}
                      />
                    </div>
                    <div className="col-12 col-sm-12 col-md">
                      <Select
                        name="phone_code"
                        color="gray"
                        options={CELLPHONE_CODE}
                        label="Código de Teléfono"
                        onChange={this.handleChangeSelect("form")}
                        valueSelect={form.phone_code}
                      />
                    </div>
                    <div className="col-12 col-sm-12 col-md">
                      <Input
                        color="gray"
                        name="phone"
                        label="Teléfono"
                        type="number"
                        onChange={this.handleChange("form")}
                        value={form.phone}
                      />
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-12 col-sm-12 col-md">
                      <Select
                        name="coach"
                        color="gray"
                        options={BOOLEAN_STATUS}
                        label="Coach"
                        onChange={this.handleChangeSelect("form")}
                        valueSelect={form.coach}
                      />
                    </div>
                    <div className="col-12 col-sm-12 col-md">
                      <Select
                        name="status"
                        color="gray"
                        options={STATUS_USER}
                        label="Estatus"
                        onChange={this.handleChangeSelect("form")}
                        valueSelect={form.status}
                      />
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-12 col-sm-12 col-md">
                      <Input
                        color="gray"
                        name="email"
                        label="E-mail"
                        type="text"
                        onChange={this.handleChange("form")}
                        maxlength={90}
                        value={form.email}
                      />
                    </div>
                    <div className="col-12 col-sm-12 col-md">
                      <Input
                        color="gray"
                        name="password"
                        label="Contraseña"
                        type="password"
                        onChange={this.handleChange("form")}
                        maxlength={90}
                      />
                    </div>
                    <div className="col-12 col-sm-12 col-md">
                      <Input
                        color="gray"
                        name="password_confirmation"
                        label="Confirmar contraseña"
                        type="password"
                        onChange={this.handleChange("form")}
                        maxlength={90}
                      />
                    </div>
                  </div>
                  <div className="text-center row">
                    <div className="col col-sm">
                      <Button
                        submitted={isSubmit}
                        block
                        type="submit"
                        className="btn-orange shadow"
                      >
                        Guardar
                      </Button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default DoctorEdit;
