import { User } from "models";

export type State = {
  isSubmit: boolean;
  error: string;
  user: User;
};

export const initialState: State = {
  isSubmit: false,
  error: "",
  user: {
    id: 0,
    alphanumeric: "",
    user_id: 0,
    role: 0,
    email: "",
    coach: 0,
    status: 0,
    activated: 0,
    forms: 0,
    remember_token: "",
    created_at: new Date(),
    updated_at: new Date(),
    deleted_at: new Date(),
  },
};
