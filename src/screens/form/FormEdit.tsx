import React, { ReactNode } from "react";
import { RouteComponentProps } from "react-router-dom";
import axios, { CancelTokenSource } from "axios";

import { User } from "models";
import { DoctorService } from "services";

import { initialState, State } from "./FormState";
import { Input, PageTitle, TitleModal, Button } from "components";
import { handlerError, showSuccess } from "utils";

interface MatchParams {
  id?: string;
}

type Props = RouteComponentProps<MatchParams>;

class FormEdit extends React.PureComponent<Props, State> {
  public state: State;
  public source: CancelTokenSource;

  constructor(props: Props) {
    super(props);

    this.state = initialState;

    const cancelToken = axios.CancelToken;
    this.source = cancelToken.source();
  }

  componentDidMount() {
    const cancelToken = axios.CancelToken;
    this.source = cancelToken.source();

    this.getUserFormData();
  }

  componentWillUnmount() {
    this.source.cancel("Cancel by user");
  }

  getUserFormData = async () => {
    const { id } = this.props.match.params;

    if (!!id) {
      try {
        const doctor = await DoctorService.getById(parseInt(id), this.source);
        const { person } = doctor;

        if (!!person) {
          const parseDoctor: User = {
            ...doctor,
            ...person,
            id: doctor.id,
          };

          console.log("doctor", parseDoctor);

          this.setState({
            user: parseDoctor,
          });
        }
      } catch (err) {
        handlerError(err);
      }
    }
  };

  handleChange = (key: string) => {
    return (event: React.ChangeEvent<HTMLInputElement>) => {
      const { value, name } = event.currentTarget;

      this.setState((prevState: any) => {
        return {
          ...prevState,
          [key]: {
            ...prevState[key],
            [name]: value,
          },
        };
      });
    };
  };

  handleSubmit = async (
    event: React.FormEvent<HTMLFormElement>
  ): Promise<void> => {
    event.preventDefault();

    if (this.state.isSubmit) {
      return;
    }

    const { user } = this.state;

    if (!!user) {
      this.setState({
        isSubmit: true,
      });

      DoctorService.usage(user)
        .then(() => {
          showSuccess();

          setTimeout(() => {
            this.props.history.push("/admin/doctors");
          }, 1000);
        })
        .catch(handlerError)
        .finally(() => this.setState({ isSubmit: false }));
    }
  };

  render(): ReactNode {
    const { isSubmit, user } = this.state;

    return (
      <div className="container">
        <PageTitle backUrl="/admin/doctors">Registro de Formularios</PageTitle>
        <div className="row justify-content-center">
          <div className="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
            <TitleModal>Uso de Formulario</TitleModal>
            <div className="card">
              <div className="card-body">
                <form
                  onSubmit={this.handleSubmit}
                  noValidate
                  autoComplete="off"
                >
                  <div className="row">
                    <div className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                      <Input
                        color="gray"
                        name="forms"
                        label="Límite máximo de uso"
                        type="number"
                        onChange={this.handleChange("user")}
                        value={user.forms}
                      />
                    </div>
                  </div>
                  <div className="text-center row">
                    <div className="col col-sm">
                      <Button
                        submitted={isSubmit}
                        block
                        type="submit"
                        className="btn-orange shadow"
                      >
                        Guardar
                      </Button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default FormEdit;
