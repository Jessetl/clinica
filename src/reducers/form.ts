import { FormAction, FormState } from "actions";
import { Reducer } from "redux";

export const form: Reducer<FormState, FormAction> = (state = null, action) => {
  switch (action.type) {
    case "Form/SET":
      return action.form;
    case "Form/REMOVE":
      return null;
    default:
      return state;
  }
};
