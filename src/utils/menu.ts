import ROLES from "./RoleId";

export const SidebarMenu = [
  {
    display_name: "Historias",
    path: "/admin/stories",
    roles: [ROLES.ROOT, ROLES.DOCTOR, ROLES.COACH],
    submodules: [],
  },
  {
    display_name: "Profesionales",
    path: "/admin/doctors",
    roles: [ROLES.ROOT],
    submodules: [],
  },
  {
    display_name: "Perfil",
    path: "/admin/profile",
    roles: [ROLES.ROOT, ROLES.DOCTOR, ROLES.COACH],
    submodules: [],
  },
];
