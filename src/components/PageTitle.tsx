import React from "react";
import { Link } from "react-router-dom";

import goBack from "assets/icons/back.svg";

interface PageTitleProps
  extends React.DetailedHTMLProps<
    React.HTMLAttributes<HTMLDivElement>,
    HTMLDivElement
  > {
  backUrl?: string;
}

export default function PageTitle({
  children,
  className,
  backUrl,
}: PageTitleProps) {
  return (
    <div className={`page-title ${className ? className : ""} text-uppercase`}>
      {backUrl && (
        <Link to={backUrl}>
          <img src={goBack} width="20" height="20" />
        </Link>
      )}
      {children}
    </div>
  );
}
