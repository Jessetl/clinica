import { Metabolizer, TestForms } from "./formModel";
import { User } from "./userModel";

export type Test = {
  id: number;
  user_id: number;
  type: number;
  gender: number;
  chronological: number;
  biophysical: number;
  biochemical: number;
  differential: number;
  orthomolecular: number;
  type_name: string;
  age_type: string;
  forms: TestForms[];
  metabolizer: Metabolizer;
  user: User;
  created_at: Date;
  updated_at: Date;
};

export interface Tests {
  current_page: number;
  last_page: number;
  data: Test[];
}

export interface Age {
  value: number;
  age: number;
}
