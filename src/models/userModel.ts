import { Person } from "./personModel";
import { FormModel } from "./formModel";

export type User = {
  id: number;
  alphanumeric: string;
  user_id: number;
  role: number;
  email: string;
  coach: number;
  forms: number;
  status: number;
  activated: number;
  person?: Person;
  user?: User;
  remaining_biophysics?: number;
  remaining_biochemistry?: number;
  remaining_orthomolecular?: number;
  remaining_genetics?: number;
  remember_token: string;
  created_at: Date;
  updated_at: Date;
  deleted_at: Date;
};

export interface Users {
  current_page: number;
  last_page: number;
  data: User[];
}

export type UserForm = {
  id: number;
  names: string;
  surnames: string;
  phone_code: string;
  phone: string;
  password: string;
  password_confirmation?: string;
};
