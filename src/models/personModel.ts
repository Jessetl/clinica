import { City } from "./cityModel";

export type Person = {
  id: number;
  user_id: number;
  city_id: number;
  document: string;
  identification_id: string;
  history: Date;
  surnames: string;
  names: string;
  birthday: Date;
  age: number;
  gender: number;
  birthplace: string;
  phone_code: string;
  phone: string;
  cellphone_code: string;
  cellphone: string;
  marital_status: number;
  occupation: string;
  address: string;
  observations: string;
  city: City;
  history_es: Date;
  updated_at_es: Date;
  full_name: string;
  full_identification: string;
  created_at: Date;
  updated_at: Date;
};
