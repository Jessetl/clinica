import { Board } from "./boardModel";

export type Range = {
  id: number;
  min: number;
  max: number;
  boards: Board[];
  created_at: Date;
  updated_at: Date;
};
